# README #
Lung Texture Classifier Using Deep Learning. 
To run inference, simply use:
```shell
python run_inference.py --config_path config_inference.json
```
Here the `config_inference.json` contains the data, model and inference hyperparameters. 

### Installation

For environment requirements, see `requirements.txt`.
```text
dicomhd@git+ssh://git@bitbucket.org/invenshure/dicomhd.git
alive-progress==2.1.0
matplotlib==3.5.1
MedPy==0.4.0
tensorflow==2.7.0
```
### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact