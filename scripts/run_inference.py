import argparse
import os
import sys
from time import sleep
import numpy as np
import tensorflow as tf
from alive_progress import alive_bar
from dicomhd import io as dhd_io
from medpy.io import load as load_lbl

sys.path.append('../')

from deeplta.config import Config
from deeplta.model.model import DeepLTA
from deeplta.preprocessing.image_2d import clip
from deeplta.preprocessing.image_2d import normalize


def DeepLTA_classifier(
        data,
        lung_mask,
        config,
):
    """Run  texture classifier on input data
    Arguments:
        data: dcmseris.pixeldata
            image volume pixel data: shape(D,H,W), ex:(385,512,512)
        lung_mask:
            lung volume mask: shape(D,H,W), ex:(385,512,512)
        config: deeplta.config.Config
            Config object which contains data, train, predict and model hyper-parameters
    Returns:
        voxel_data : output of the voxel texture classifier as a 3D volume.
        Texture labels={0:background,
                        1: normal,
                        2:hyperlucent,
                        3: groung glass,
                        4: reticular,
                        5:honeycomb,
                        6: consolidation}
    """

    strides = [1, 1]
    padding = 'SAME'

    patch_size = config.model['input_shape'][:-1]
    batch_size = config.predict['batch_size']
    clip_min_value = config.data['clip_min_value']
    clip_max_value = config.data['clip_max_value']

    # load deeplta model
    model = DeepLTA(config)

    # lung mask
    lung_mask[lung_mask == 3] = 0 # mask out airways
    # get the indices of lung mask
    idx_with_ann = np.ma.where(lung_mask.sum(axis=(1, 2)) > 1024)[0]
    # get the axial slices in lung mask
    slices = data[idx_with_ann]
    anns = lung_mask[idx_with_ann]

    # iterate over every axial slice, generate image patch, predict the patch label
    # and assign the label to centroid pixel

    # array to store the predicted voxel data
    voxel_data = tf.zeros_like(data).numpy()
    sleep(1)
    with alive_bar(slices.shape[0], force_tty=True) as bar:
        for i in range(0, slices.shape[0]):
            # print(f'{i}/{n_imgs_with_ann}')
            ann_i = anns[[i]]
            slice_i = slices[[i]]
            dim = len(slice_i.shape)

            if dim == 3:
                slice_i = np.expand_dims(slice_i, [-1])
                ann_i = np.expand_dims(ann_i, [-1])
            elif dim == 2:
                slice_i = np.expand_dims(slice_i, [0, -1])
                ann_i = np.expand_dims(ann_i, [0, -1])
            else:
                raise ValueError('Inputs imgs can only be onf dim 2 or 3.')
            # extract image patch
            patches_i = tf.image.extract_patches(
                slice_i,
                sizes=[1, *patch_size, 1],
                strides=[1, *strides, 1],
                rates=[1, 1, 1, 1],
                padding=padding
            )
            anns_i = tf.image.extract_patches(
                ann_i,
                sizes=[1, *patch_size, 1],
                strides=[1, *strides, 1],
                rates=[1, 1, 1, 1],
                padding=padding
            )
            patches_i = tf.reshape(patches_i, (-1, *patch_size, 1))
            anns_i = tf.reshape(anns_i, (-1, *patch_size, 1))
            indices_i = tf.constant(list(range(patches_i.shape[0])), dtype=tf.int32)
            center_pixel_ann = anns_i[:, patch_size[0] // 2 + 1, patch_size[0] // 2 + 1, 0]
            # mask-out non lung region
            mask = center_pixel_ann > 0
            patches_with_ann_i = patches_i[mask]
            indices_with_ann_i = indices_i[mask]

            # clip and normalize patch pixel value
            patches_with_ann_i = clip(
                patches_with_ann_i,
                min_value=clip_min_value,
                max_value=clip_max_value
            )
            patches_with_ann_i = normalize(
                patches_with_ann_i,
                method='min-max',
                min_value=clip_min_value,
                max_value=clip_max_value
            )

            # predict label
            patch_probs_with_ann_i = model.predict(
                patches_with_ann_i,
                batch_size,
                verbose=0,
                workers=2,
                use_multiprocessing=False
            )

            patch_lbls_with_ann_i = np.argmax(patch_probs_with_ann_i, axis=1) + 1
            lbls_i = tf.zeros(shape=(patches_i.shape[0]))
            # update the corresponding index label
            lbls_i = tf.tensor_scatter_nd_add(
                lbls_i,
                indices=tf.expand_dims(indices_with_ann_i, -1),
                updates=patch_lbls_with_ann_i
            )

            lbls_i = tf.reshape(lbls_i, ann_i.shape)
            lbls_c = lbls_i[0, ..., 0]
            voxel_data[idx_with_ann[i], ...] = lbls_c

            bar()
    # uni8 datatype is required for segstation
    voxel_data = voxel_data.astype(np.uint8)

    return voxel_data


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config_path',
        default="config.json",
        type=str,
        help='the path to dicom series'
    )

    args = parser.parse_args()
    config = Config.from_json(args.config_path)

    dcm_series = dhd_io.read_series(config.data["dcm_series_path"])
    data = dcm_series.pixel_data
    mask, mask_header = load_lbl(config.data["lung_mask_path"])
    mask = np.swapaxes(mask, 0, 2)
    voxel_data = DeepLTA_classifier(
        data,
        mask,
        config
    )
