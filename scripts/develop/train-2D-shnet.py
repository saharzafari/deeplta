import sys
sys.path.append('../..')
import os
import random

import numpy as np
import tensorflow as tf

from tfvpc.preprocessing.two_dim import load_batch_dataset
from tfvpc.preprocessing.two_dim import count_tfrecord_examples
from tfvpc.model.shnet2D import ShadowNet

w_path = '../checkpoints/'
m_path = 'shnet-2D-5cls-03.h5'
ch_path = '../checkpoints/shnet-2D-5cls-03_checkpoint.h5'
out_path = '../data/tfrecods/'

w_path = '../checkpoints/'
out_path = '../data/tfrecods/'
out_train_fname = 'train-2D-5cls-03'
out_val_fname = 'test-2D-5cls-02'
out_train_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')
out_val_ffname = os.path.join(out_path, out_val_fname + '.tfrecords')

n_rows = 32
n_cols = 32
n_classes = 5
vocab = [2,3,4,5,6]
n_channel = 1
epoch = 200
batch_size = 128
lr = 1e-5
seed = 2020
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)

# load train data
train_dataset = load_batch_dataset(epoch,
                                   batch_size,
                                   out_train_ffname,
                                   vocab,
                                   n_classes,
                                   aug=False,
                                   train=True)

print(f'nm of training samples: {count_tfrecord_examples(out_train_ffname)}')
# load val data
val_dataset = load_batch_dataset(epoch,
                                 batch_size,
                                 out_val_ffname,
                                 vocab,
                                 n_classes,
                                 aug=False,
                                 train=True)

print(f'nm of validation samples: {count_tfrecord_examples(out_val_ffname)}')

params_model = dict(
    img_width=n_rows,
    img_height=n_cols,
    n_channels=1,
    n_classes=n_classes,
    n_filters = 8,
    batch_normalization=True,
    class_names=['0', '1', '2','3','4'],
)
shnet = ShadowNet(**params_model)
shnet.summary()

#metrics
metrics = [
    tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
    tf.keras.metrics.Precision(name='precision'),
    tf.keras.metrics.Recall(name='recall'),
    tf.keras.metrics.AUC(name='auc'),
    tf.keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]
#compile model
opt = tf.keras.optimizers.Adam(learning_rate=lr)
shnet.compile(optimizer=opt,
               loss=tf.keras.losses.categorical_crossentropy,
               metrics=metrics)
#history
history = shnet.fit(
    train_dataset,
    val_dataset,
    epoch,
    steps_per_epoch = int(np.ceil(count_tfrecord_examples(out_train_ffname) / batch_size)),
    validation_steps = int(np.ceil(count_tfrecord_examples(out_val_ffname) / batch_size)),
#     class_weight=weight,
    callbacks=[
     tf.keras.callbacks.TensorBoard(
         log_dir='logs',
         write_graph=True
     ),
     tf.keras.callbacks.EarlyStopping(
        # Stop training when `val_loss` is no longer improving
     monitor="val_loss",
        # "no longer improving" being defined as "no better than 1e-2 less"
     min_delta=1e-2,
        # "no longer improving" being further defined as "for at least 2 epochs"
     patience=20,
     verbose=1,
    ),
     tf.keras.callbacks.ModelCheckpoint(
         mode='max',
         filepath=ch_path,
         monitor='val_accuracy',
         save_best_only='True',
         save_weights_only='True',
         verbose=1
     )
    ]
)

#save model
print('train completed!')
unet.save_weights(os.path.join(w_path, m_path), overwrite=True)

print('save completed!')







