import sys
sys.path.append('../..')
import os
import random

import numpy as np
import tensorflow as tf
from tensorflow_addons.metrics import F1Score
from tensorflow_addons.losses import SigmoidFocalCrossEntropy


from tfvpc.preprocessing.two_dim import load_batch_dataset
from tfvpc.preprocessing.two_dim import load_dataset
from tfvpc.preprocessing.two_dim import count_tfrecord_examples
from tfvpc.preprocessing.two_dim import class_weight
from tfvpc.model.resnet2D import Resnet2DBuilder
# from tfvpc.model.ltanet2D import LTA2DBuilder
# from tfvpc.model.resnet2D_lta import Resnet2DBuilder
from tfvpc.utils.vis import log_confusion_matrix
gpus = tf.config.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)
    
w_path = '../checkpoints/'
model_fname = '../checkpoints/resnet05-2D-6cls_checkpoint-01.h5'
# m_path = 'resnet34-2D-6cls-12.h5'
ch_path = '../checkpoints/resnet05-2D-6cls_checkpoint-01.h5'
out_path = '../data/tfrecods/'
out_train_fname = 'train-2D-6cls-01'
out_val_fname = 'val-2D-6cls-01'
out_train_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')
out_val_ffname = os.path.join(out_path, out_val_fname + '.tfrecords')
#params
n_rows = 64
n_cols = 64
n_classes = 6
vocab = [1,2,3,4,5,6]
n_channel = 1
epoch = 200
batch_size = 128
lr = 1e-5
seed = 2020
retrain=False
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)

# load train data
train_dataset = load_batch_dataset(epoch,
                                   batch_size,
                                   out_train_ffname,
                                   vocab,
                                   n_classes,
                                   aug = False,
                                   train=True,
                                   normalize= True)
print(f'nm of training samples: {count_tfrecord_examples(out_train_ffname)}')
# load val data
val_dataset = load_batch_dataset(epoch,
                                 batch_size,
                                 out_val_ffname,
                                 vocab,
                                 n_classes,
                                 aug=False,
                                 train=True, 
                                 normalize=True)
print(f'nm of validation samples: {count_tfrecord_examples(out_val_ffname)}')

t_dataset = load_dataset(out_train_ffname)
weight = class_weight(t_dataset, n_classes)
weight = {i: weight[i] for i in range(n_classes)}
print('class weights are:', weight)

# build model
# model = Resnet2DBuilder.build_resnet_07((n_rows, n_cols, n_channel), n_classes)
# model = LTA2DBuilder.build_lta((n_rows, n_cols, n_channel), n_classes)
model = Resnet2DBuilder.build_resnet_05((n_rows, n_cols, n_channel), n_classes)
model.summary()
#metrics
metrics = [
    F1Score(n_classes, name='F1'),
    tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
    tf.keras.metrics.Precision(name='precision'),
    tf.keras.metrics.Recall(name='recall'),
    tf.keras.metrics.AUC(name='auc'),
    tf.keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]
# retrain
if retrain:
    model.load_weights(model_fname)
# lr schdular
# initial_learning_rate = lr
# decay_steps = (epoch * count_tfrecord_examples(out_train_ffname)) / batch_size
# lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
#     initial_learning_rate,
#     decay_steps=decay_steps,
#     decay_rate=0.96,
#     staircase=True)
opt = tf.keras.optimizers.Adam(learning_rate=lr)
#compile model

model.compile(optimizer=opt,
               loss=tf.keras.losses.categorical_crossentropy,
               metrics=metrics)
if retrain:
    resnet.load_weights(model_fname)
  #history
logdir='logs'
file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')
history = model.fit(
    train_dataset,
    validation_data=val_dataset,
    epochs=epoch,
    steps_per_epoch = int(np.ceil(count_tfrecord_examples(out_train_ffname) / batch_size)),
    validation_steps = int(np.ceil(count_tfrecord_examples(out_val_ffname) / batch_size)),
#     class_weight=weight,
    callbacks=[
     tf.keras.callbacks.TensorBoard(
         log_dir=logdir,
         histogram_freq=1,
         write_images=True,
         write_graph=False,
         update_freq='epoch',
         profile_batch=2,
         embeddings_freq=1
     ),
     tf.keras.callbacks.EarlyStopping(
        # Stop training when `val_loss` is no longer improving
        monitor="val_loss",
        # "no longer improving" being defined as "no better than 1e-2 less"
        min_delta=1e-4,
        # "no longer improving" being further defined as "for at least 2 epochs"
        patience=100,
        verbose=1,
    ),
     tf.keras.callbacks.ModelCheckpoint(
         mode='max',
         filepath=ch_path,
         monitor='val_recall',
         save_best_only='True',
         save_weights_only='True',
         verbose=1
     ),
#      tf.keras.callbacks.LambdaCallback(on_epoch_end=log_confusion_matrix),
    ]
)


#save model
print('train completed!')
# np.save('../checkpoints/history-resnet10-2D-6cls-02.npy', history.history)
# resnet.save_weights(os.path.join(w_path, m_path), overwrite=True)

print('save completed!')





