import sys
sys.path.append('../..')
import os
import random

import numpy as np
import tensorflow as tf
from tensorflow_addons.metrics import F1Score
from tensorflow_addons.losses import SigmoidFocalCrossEntropy


from tfvpc.preprocessing.two_dim import load_batch_dataset
from tfvpc.preprocessing.two_dim import load_train_val_dataset
from tfvpc.preprocessing.two_dim import count_tfrecord_examples
from tfvpc.preprocessing.two_dim import class_weight
from tfvpc.model.resnet2D import Resnet2DBuilder
from tfvpc.utils.vis import log_confusion_matrix

w_path = '../checkpoints/'
model_fname = '../checkpoints/resnet34-2D-6cls_checkpoint.h5'
m_path = 'resnet34-2D-6cls-04-std.h5'
ch_path = '../checkpoints/resnet34-2D-6cls_checkpoint-04-std.h5'
out_path = '../data/tfrecods/'
out_train_fname = 'train-2D-6cls-04'
out_train_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')
#params
n_rows = 32
n_cols = 32
n_classes = 6
vocab = [1,2,3,4,5,6]
n_channel = 1
epoch = 200
batch_size = 128
lr = 1e-5
seed = 2020
retrain=False
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)

# load train data
train_dataset, val_dataset, t_size, v_size = load_train_val_dataset(epoch,
                                                                    batch_size, 
                                                                    out_train_ffname,
                                                                    vocab, 
                                                                    n_classes,
                                                                    train=False)
print('train samples:', t_size)
print('val samples:', v_size)

# build model
resnet = Resnet2DBuilder.build_resnet_34((n_rows, n_cols, n_channel), n_classes)
resnet.summary()
#metrics
metrics = [
    F1Score(n_classes, name='F1'),
    tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
    tf.keras.metrics.Precision(name='precision'),
    tf.keras.metrics.Recall(name='recall'),
    tf.keras.metrics.AUC(name='auc'),
    tf.keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]
# retrain
if retrain:
    resnet.load_weights(model_fname)

#compile model
opt = tf.keras.optimizers.Adam(learning_rate=lr)
resnet.compile(optimizer=opt,
               loss=tf.keras.losses.categorical_crossentropy,
               metrics=metrics)
if retrain:
    resnet.load_weights(model_fname)
  #history
logdir='logs'
file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')
history = resnet.fit(
    train_dataset,
    validation_data=val_dataset,
    epochs=epoch,
    steps_per_epoch = int(np.ceil(t_size / batch_size)),
    validation_steps = int(np.ceil(v_size / batch_size)),
#     class_weight=weight,
    callbacks=[
     tf.keras.callbacks.TensorBoard(
         log_dir=logdir,
         histogram_freq=1,
         write_images=True,
         write_graph=False,
         update_freq='epoch',
         profile_batch=2,
         embeddings_freq=1
     ),
     tf.keras.callbacks.EarlyStopping(
        # Stop training when `val_loss` is no longer improving
        monitor="val_loss",
        # "no longer improving" being defined as "no better than 1e-2 less"
        min_delta=1e-3,
        # "no longer improving" being further defined as "for at least 2 epochs"
        patience=20,
        verbose=1,
    ),
     tf.keras.callbacks.ModelCheckpoint(
         mode='max',
         filepath=ch_path,
         monitor='val_recall',
         save_best_only='True',
         save_weights_only='True',
         verbose=1
     ),
#      tf.keras.callbacks.LambdaCallback(on_epoch_end=log_confusion_matrix),
    ]
)


#save model
print('train completed!')
# np.save('../checkpoints/history-resnet10-2D-6cls-02.npy', history.history)
resnet.save_weights(os.path.join(w_path, m_path), overwrite=True)

print('save completed!')
