import os
import sys
sys.path.append('../..')
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from dicomhd import io as dhd_io
from medpy.io import load as load_lbl

from tfvpc.preprocessing.two_dim import extract_2d_patch
import os
import sys
sys.path.append('../..')
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np
import matplotlib.pyplot as plt
from dicomhd import io as dhd_io
from medpy.io import load as load_lbl

from tfvpc.preprocessing.two_dim import extract_2d_patch
from tfvpc.preprocessing.two_dim import extract_2d_patch_labels
from tfvpc.preprocessing.two_dim import extract_annotated_2d_patch
from tfvpc.preprocessing.two_dim import _int64_feature
from tfvpc.preprocessing.two_dim import _bytes_feature
# params
n_rows = 32
n_cols = 32
strides = 3
ratio_th = 0.6
test = False
store_as_tfrecord = True
store_as_dataset = False
# path to data
path_test_img = '../../data/dev/test/input/complete/'
path_test_lbl = '../../data/dev/test/output/complete/'

path_train_img = '../../data/dev/train/input/complete/'
path_train_lbl = '../../data/dev//train/output/complete/'

path_val_img = '../../data/dev/val/input/complete/'
path_val_lbl = '../../data/dev/val/output/complete/'

out_train_fname = 'train-2D-6cls-st3'
out_val_fname = 'val-2D-6cls-st3'
out_test_fname = 'test-2D-6cls-st3'
out_path = '../data/tfrecods/'
# 04: hyper=0.8 and rest 0.6, skip gg==1
# 06: patch size: 64, hyper=0.8, rest =0.6

with tf.device('/cpu:0'):
    if store_as_tfrecord:
        out_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')
    
    img_fnames = sorted([f for f in os.listdir(path_train_img)])
    img_fnames.remove('.DS_Store')
    list_img_patches = []
    lbl_fnames = sorted([f for f in os.listdir(path_train_lbl)])
    lbl_fnames.remove('.DS_Store')
    list_lbl_patches = []
    
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    if store_as_tfrecord:
        writer = tf.io.TFRecordWriter(out_ffname)
    n_samples = 0
    for img_fname, lbl_fname in zip(img_fnames, lbl_fnames):
#         if 'Normal' in img_fname:
#             continue
        print(img_fname, ',',lbl_fname)
        # extract lable patches
        lbl_file_path = os.path.join(path_train_lbl, lbl_fname)
        lbl_file_path = os.path.join(lbl_file_path, 'annotation.gipl')
        lbl, label_header = load_lbl(lbl_file_path)
        lbl = np.swapaxes(lbl,0,2)
        if lbl.sum() > 0:
            # get indices for voxels with ann
            idx_with_ann = np.ma.where(lbl.sum(axis=(1,2)) > 0)[0]
            # crop label
            lbl_ann = lbl[idx_with_ann, ...]
            print('label is:', lbl_ann.max())
            if 'groundglass' in img_fname and lbl_ann.max() == 1:
                continue
                mask_gg = lbl_ann==1
                lbl_ann[mask_gg] = 3
                print('label is:', lbl_ann.max())
            tlbl_patches = extract_2d_patch(lbl_ann, n_rows, n_cols, strides)
            # load image
            img_file_path = os.path.join(path_train_img, img_fname)
            img = dhd_io.read_series(img_file_path)
            # crop image
            img_ann=img.pixel_data[idx_with_ann, ...]
            # extract image patches
            timg_patches = extract_2d_patch(img_ann, n_rows, n_cols, strides)
            # extract patch with annotation
            if 'Hyperlucent' in img_fname:
                ratio_th=0.9
            else:
                ratio_th=0.7
            print(ratio_th)
            timg_patches_with_ann, tlbl_patches_with_ann, tpatch_lbls_with_ann = \
                extract_annotated_2d_patch(timg_patches,
                                           tlbl_patches,
                                           n_rows,
                                           n_cols,
                                           ratio_th)
            if store_as_dataset:
                list_lbl_patches.append(tlbl_patches)
                list_img_patches.append(timg_patches)
            
            if store_as_tfrecord:
                for patch_idx in range(timg_patches_with_ann.shape[0]):
            
                    timg_patch_idx = timg_patches_with_ann[patch_idx, ...]
                    tlbl_patch_idx = tlbl_patches_with_ann[patch_idx, ...]
                    tpatch_lbl_idx = tpatch_lbls_with_ann[patch_idx, ...]
                    example = tf.train.Example(
                        features=tf.train.Features(feature={
                            'height': _int64_feature(n_rows),
                            'width': _int64_feature(n_cols),
                            'lbl': _int64_feature(int(tpatch_lbl_idx.numpy())),
                            'img': _bytes_feature(tf.io.serialize_tensor(timg_patch_idx).numpy()),
                        })
                                              )
                    writer.write(example.SerializeToString())
                                  
            n_samples += timg_patches_with_ann.shape[0]
#             print('image dim:', img.shape, 'label dim:', lbl.shape)
            print(f'patch dim: {timg_patches_with_ann.shape}, patch lbl dim: {tlbl_patches_with_ann.shape}')
            print('-' * 50)
            
    if store_as_tfrecord:
        writer.close()
         
    if store_as_dataset:
        # read all labeld images from directory and generate label patches
        stack_img_patches = tf.concat(list_img_patches, axis=0)
        stack_lbl_patches = tf.concat(list_lbl_patches, axis=0)
        print(f'shape of stack images: {stack_img_patches.shape}')
        print(f'shape of stack labels: {stack_lbl_patches.shape}')
print('Training samples', n_samples)