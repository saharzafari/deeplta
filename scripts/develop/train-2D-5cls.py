import sys
sys.path.append('../..')
import os
import random

import numpy as np
import tensorflow as tf
from tensorflow_addons.metrics import F1Score
from tensorflow_addons.losses import SigmoidFocalCrossEntropy


from tfvpc.preprocessing.two_dim import load_batch_dataset
from tfvpc.preprocessing.two_dim import load_dataset
from tfvpc.preprocessing.two_dim import count_tfrecord_examples
from tfvpc.preprocessing.two_dim import class_weight
from tfvpc.model.resnet2D import Resnet2DBuilder
from tfvpc.utils.vis import log_confusion_matrix

w_path = '../checkpoints/'
m_path = 'resnet10-2D-5cls-.h5'
ch_path = '../checkpoints/resnet10-2D-5cls-_checkpoint.h5'
out_path = '../data/tfrecods/'

w_path = '../checkpoints/'
out_path = '../data/tfrecods/'
out_train_fname = 'train-2D-5cls-03'
out_val_fname = 'test-2D-5cls-02'
out_train_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')
out_val_ffname = os.path.join(out_path, out_val_fname + '.tfrecords')

n_rows = 32
n_cols = 32
n_classes = 5
vocab = [2,3,4,5,6]
n_channel = 1
epoch = 200
batch_size = 128
lr = 1e-5
seed = 2020
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)

# load train data
train_dataset = load_batch_dataset(epoch,
                                   batch_size,
                                   out_train_ffname,
                                   vocab,
                                   n_classes,
                                   aug=False,
                                   train=True)
print(f'nm of training samples: {count_tfrecord_examples(out_train_ffname)}')
# load val data
val_dataset = load_batch_dataset(epoch,
                                 batch_size,
                                 out_val_ffname,
                                 vocab,
                                 n_classes,
                                 aug=False,
                                 train=True)
print(f'nm of validation samples: {count_tfrecord_examples(out_val_ffname)}')

# t_dataset = load_dataset(out_train_ffname)
# weight = class_weight(t_dataset, n_classes)
# weight = {i: weight[i] for i in range(n_classes)}
# print('class weights are:', weight)

# build model
resnet = Resnet2DBuilder.build_resnet_10((n_rows, n_cols, n_channel), n_classes)
resnet.summary()
#metrics
metrics = [
    F1Score(n_classes, name='F1'),
    tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
    tf.keras.metrics.Precision(name='precision'),
    tf.keras.metrics.Recall(name='recall'),
    tf.keras.metrics.AUC(name='auc'),
    tf.keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]
#lr schduler
initial_learning_rate = lr
lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate, decay_steps=100000, decay_rate=0.96, staircase=True
)
opt = tf.keras.optimizers.Adam(learning_rate=lr)
#compile model
resnet.compile(optimizer=opt,
               loss=tf.keras.losses.categorical_crossentropy,
#                loss = SigmoidFocalCrossEntropy(reduction=tf.keras.losses.Reduction.AUTO),
               metrics=metrics)
#history
logdir='logs'
file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')
history = resnet.fit(
    train_dataset,
    validation_data=val_dataset,
    epochs=epoch,
    steps_per_epoch = int(np.ceil(count_tfrecord_examples(out_train_ffname) / batch_size)),
    validation_steps = int(np.ceil(count_tfrecord_examples(out_val_ffname) / batch_size)),
#     class_weight=weight,
    callbacks=[
     tf.keras.callbacks.TensorBoard(
         log_dir=logdir,
         histogram_freq=1,
         write_images=True,
         write_graph=False,
         update_freq='epoch',
         profile_batch=2,
         embeddings_freq=1
     ),
     tf.keras.callbacks.EarlyStopping(
        # Stop training when `val_loss` is no longer improving
        monitor="val_loss",
        # "no longer improving" being defined as "no better than 1e-2 less"
        min_delta=1e-2,
        # "no longer improving" being further defined as "for at least 2 epochs"
        patience=20,
        verbose=1,
    ),
     tf.keras.callbacks.ModelCheckpoint(
         mode='max',
         filepath=ch_path,
         monitor='val_recall',
         save_best_only='True',
         save_weights_only='True',
         verbose=1
     ),
#      tf.keras.callbacks.LambdaCallback(on_epoch_end=log_confusion_matrix),
    ]
)


#save model
print('train completed!')
resnet.save_weights(os.path.join(w_path, m_path), overwrite=True)

print('save completed!')




