import sys
sys.path.append('../..')
import os
import random

import numpy as np
import tensorflow as tf
from tensorflow_addons.metrics import F1Score
from tensorflow_addons.losses import SigmoidFocalCrossEntropy


from tfvpc.preprocessing.three_dim import load_batch_dataset
from tfvpc.preprocessing.three_dim import load_dataset
from tfvpc.preprocessing.three_dim import count_tfrecord_examples
from tfvpc.preprocessing.three_dim import class_weight
from tfvpc.model.resnet3D import Resnet3DBuilder
from tfvpc.utils.vis import log_confusion_matrix

w_path = '../checkpoints/'
model_fname = '../checkpoints/resnet05-3D-6cls_checkpoint-05.h5'
ch_path = '../checkpoints/resnet05-3D-6cls_checkpoint-05.h5'
out_path = '../data/tfrecods/'
out_train_fname = 'train-3D-6cls-05'
out_val_fname = 'val-3D-6cls-05'
out_train_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')
out_val_ffname = os.path.join(out_path, out_val_fname + '.tfrecords')
#params
n_rows = 64
n_cols = 64
n_planes = 16
n_channel = 1
n_classes = 6
vocab = [1,2,3,4,5,6]
epoch = 200
batch_size = 128
lr = 1e-5
seed = 2020
retrain=True
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)

# load train data
train_dataset = load_batch_dataset(epoch,
                                   batch_size,
                                   out_train_ffname,
                                   vocab,
                                   n_classes,
                                   aug = False,
                                   train=True,
                                   normalize= True)
print(f'nm of training samples: {count_tfrecord_examples(out_train_ffname)}')
# load val data
val_dataset = load_batch_dataset(epoch,
                                 batch_size,
                                 out_val_ffname,
                                 vocab,
                                 n_classes,
                                 aug=False,
                                 train=True, 
                                 normalize=True)
print(f'nm of validation samples: {count_tfrecord_examples(out_val_ffname)}')

# t_dataset = load_dataset(out_train_ffname)
# weight = class_weight(t_dataset, n_classes)
# weight = {i: weight[i] for i in range(n_classes)}
# print('class weights are:', weight)

# build model
resnet = Resnet3DBuilder.build_resnet_05((n_planes, n_rows, n_cols, n_channel), n_classes)
resnet.summary()
#metrics
metrics = [
    F1Score(n_classes, name='F1'),
    tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
    tf.keras.metrics.Precision(name='precision'),
    tf.keras.metrics.Recall(name='recall'),
    tf.keras.metrics.AUC(name='auc'),
    tf.keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]
# retrain
if retrain:
    resnet.load_weights(model_fname)

opt = tf.keras.optimizers.Adam(learning_rate=lr)
#compile model

resnet.compile(optimizer=opt,
               loss=tf.keras.losses.categorical_crossentropy,
               metrics=metrics)
if retrain:
    resnet.load_weights(model_fname)
  #history
logdir='logs'
history = resnet.fit(
    train_dataset,
    validation_data=val_dataset,
    epochs=epoch,
    steps_per_epoch = int(np.ceil(count_tfrecord_examples(out_train_ffname) / batch_size)),
    validation_steps = int(np.ceil(count_tfrecord_examples(out_val_ffname) / batch_size)),
#     class_weight=weight,
    callbacks=[
     tf.keras.callbacks.TensorBoard(
         log_dir=logdir,
         histogram_freq=1,
         write_images=True,
         write_graph=False,
         update_freq='epoch',
         profile_batch=2,
         embeddings_freq=1
     ),
     tf.keras.callbacks.EarlyStopping(
        # Stop training when `val_loss` is no longer improving
        monitor="val_loss",
        # "no longer improving" being defined as "no better than 1e-2 less"
        min_delta=1e-4,
        # "no longer improving" being further defined as "for at least 2 epochs"
        patience=100,
        verbose=1,
    ),
     tf.keras.callbacks.ModelCheckpoint(
         mode='max',
         filepath=ch_path,
         monitor='val_recall',
         save_best_only='True',
         save_weights_only='True',
         verbose=1
     ),
#      tf.keras.callbacks.LambdaCallback(on_epoch_end=log_confusion_matrix),
    ]
)


#save model
print('train completed!')





