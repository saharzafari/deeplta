import os
import sys
sys.path.append('../..')
import tensorflow as tf
import numpy as np
from dicomhd import io as dhd_io
from medpy.io import load as load_lbl

from tfvpc.preprocessing.three_dim import extract_3d_patch
from tfvpc.preprocessing.three_dim import extract_3d_patch_labels
from tfvpc.preprocessing.three_dim import extract_annotated_3d_patch
from tfvpc.preprocessing.three_dim import _int64_feature
from tfvpc.preprocessing.three_dim import _bytes_feature

# params
n_rows = 32
n_cols = 32
n_planes = 8
sizes = [n_planes, n_rows, n_cols]
strides = [4,4,4]
ratio_th = 0.5
method='cr'
test = True
store_as_tfrecord = True
store_as_dataset = False

# path to data
path_test_img = '../../data/dev/test/input/complete/'
path_test_lbl = '../../data/dev/test/output/complete/'

path_train_img = '../../data/dev/train/input/complete/'
path_train_lbl = '../../data/dev//train/output/complete/'

path_val_img = '../../data/dev/val/input/complete/'
path_val_lbl = '../../data/dev/val/output/complete/'

out_train_fname = 'train-3D-6cls-01'
out_val_fname = 'val-3D-6cls-01'
out_test_fname = 'test-3D-6cls-01'
out_path = '../data/tfrecods/'

#04: ratio, hyper=0.8, rest 06, skip gg, 32, stride 4
#07:centroid, stride 6
#10:centriod, stride 8, patch size = 32
#11: stride:4, patch size 32, center
#12: stride:8, patch size 32, cr

with tf.device('/cpu:0'):
    if store_as_tfrecord:
        out_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')

    img_fnames = sorted([f for f in os.listdir(path_train_img)])
    img_fnames.remove('.DS_Store')
    list_img_patches = []
    # get filename for labels
    lbl_fnames = sorted([f for f in os.listdir(path_train_lbl)])
    lbl_fnames.remove('.DS_Store')
    list_lbl_patches = []

    if not os.path.exists(out_path):
        os.makedirs(out_path)
    if store_as_tfrecord:
        # create tfrecord writer instance
        writer = tf.io.TFRecordWriter(out_ffname)
    n_samples = 0
    for img_fname, lbl_fname in zip(img_fnames, lbl_fnames):
        if 'Reticular-8f6b5a-Jude_reviewed_1-485' in img_fname:
            continue
        print(img_fname, ',',lbl_fname)
        # extract lable patches
        lbl_file_path = os.path.join(path_train_lbl, lbl_fname)
        lbl_file_path = os.path.join(lbl_file_path, 'annotation.gipl')
        lbl, label_header = load_lbl(lbl_file_path)
        lbl = np.swapaxes(lbl,0,2)
        if lbl.sum() > 0:
            # get indices for voxels with ann
            idx_with_ann = np.ma.where(lbl.sum(axis=(1,2)) > 0)[0]
            # crop label
            lbl_ann = lbl[idx_with_ann, ...]
            print('label is:', lbl_ann.max())
            if 'groundglass' in img_fname and lbl_ann.max() == 1:
                mask_gg = lbl_ann==1
                lbl_ann[mask_gg] = 3
                print('label is:', lbl_ann.max())
            tlbl_patches = extract_3d_patch(lbl_ann, sizes, strides)
#                 break
            # load image
            img_file_path = os.path.join(path_train_img, img_fname)
            img = dhd_io.read_series(img_file_path)
            # crop image
            img_ann=img.pixel_data[idx_with_ann, ...]
#                 print(img_ann.shape)
            # extract image patches
            timg_patches = extract_3d_patch(img_ann, sizes, strides)
#                 print(timg_patches.shape)
#                 break
            # extract patch with annotation
            timg_patches_with_ann, tlbl_patches_with_ann, tpatch_lbls_with_ann = \
                extract_annotated_3d_patch(timg_patches,
                                           tlbl_patches,
                                           n_planes,
                                           n_rows,
                                           n_cols,
                                           ratio_th,
                                           method=method)
            if store_as_dataset:
                list_lbl_patches.append(tlbl_patches)
                list_img_patches.append(timg_patches)

            if store_as_tfrecord:
                for patch_idx in range(timg_patches_with_ann.shape[0]):

                    timg_patch_idx = timg_patches_with_ann[patch_idx, ...]
                    tlbl_patch_idx = tlbl_patches_with_ann[patch_idx, ...]
                    tpatch_lbl_idx = tpatch_lbls_with_ann[patch_idx, ...]
                    example = tf.train.Example(
                        features=tf.train.Features(feature={
                            'height': _int64_feature(n_rows),
                            'width': _int64_feature(n_cols),
                            'depth': _int64_feature(n_planes),
                            'lbl': _int64_feature(int(tpatch_lbl_idx.numpy())),
                            'img': _bytes_feature(tf.io.serialize_tensor(timg_patch_idx).numpy()),
                        })
                                              )
                    writer.write(example.SerializeToString())

            n_samples += timg_patches_with_ann.shape[0]
            print(f'patch dim: {timg_patches_with_ann.shape}, patch lbl dim: {tlbl_patches_with_ann.shape}')
            print('-' * 50)
#                 break
    if store_as_tfrecord:
        writer.close()

    if store_as_dataset:
        # read all labeld images from directory and generate label patches
        stack_img_patches = tf.concat(list_img_patches, axis=0)
        stack_lbl_patches = tf.concat(list_lbl_patches, axis=0)
        print(f'shape of stack images: {stack_img_patches.shape}')
        print(f'shape of stack labels: {stack_lbl_patches.shape}')
print('Training samples', n_samples)