import sys

sys.path.append('../../..')
from pprint import pprint

from deeplta.config import Config
from deeplta.model.resnet2D import Resnet2D
from deeplta.dataset import TFRecordDicomDataset

config = Config.from_json('config_train_2d_6cls.json')

pprint(config.seed)
# model = Resnet2D(config)
# model.compile()
#


dataset = TFRecordDicomDataset(**config.data)

# dataset.extract_2d_patch(
#     size=(32, 32),
#     strides=4,
#     image_path='/Users/saharzafari/Documents/work/projects/lta/data/dev/test/input/complete/',
#     label_path='/Users/saharzafari/Documents/work/projects/lta/data/dev/test/output/complete/',
#     out_path='/Users/saharzafari/Documents/work/projects/lta/data/dev/test/sahar/temp.tfrecords',
#     method='center',
#     ratio_th=0.5,
#     dataset_type='tfrecords',
#     device='/cpu:0',
#
# )

train_data = dataset.load(
    path='/Users/saharzafari/Documents/work/projects/lta/data/dev/test/sahar/temp.tfrecords',
    epochs=2,
    batch_size=8,
    augment=False,
    shuffle=True,
    repeat=True,
    drop_remainder=True,
)
print(train_data)

