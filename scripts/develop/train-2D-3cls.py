import sys
sys.path.append('../..')
import os
import numpy as np
import random
import tensorflow as tf
from tfvpc.model import Resnet2DBuilder

w_path = '../checkpoints/'
out_path = '../data/tfrecods/'
out_train_fname = 'train-2D-3cls-hcr'
out_val_fname = 'val-2D-3cls-hcr'
out_train_ffname = os.path.join(out_path, out_train_fname + '.tfrecords')
out_val_ffname = os.path.join(out_path, out_val_fname + '.tfrecords')

n_rows = 32
n_cols = 32
n_classes = 3
vocab = [4,5,6]
n_channel = 1
epoch = 100
batch_size = 128
lr = 1e-5
seed = 2020
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)

def _parse_image_function(example_proto, vocab):
    image_feature_description = {
        'height': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'lbl': tf.io.FixedLenFeature([], tf.int64),
        'img': tf.io.FixedLenFeature([], tf.string),
    }

    features = tf.io.parse_single_example(example_proto, image_feature_description)
    image = tf.io.parse_tensor(features['img'], out_type=tf.float32)
    image = tf.io.decode_raw(features['img'], tf.float32)
    image.set_shape([n_rows * n_cols])
    image = tf.reshape(image, [n_rows, n_cols])

    labels = tf.cast(features['lbl'], tf.int64)
    init = tf.lookup.KeyValueTensorInitializer(
        keys=tf.constant(vocab, dtype=tf.int64), 
        values=tf.constant(list(range(len(vocab))), dtype=tf.int64)
    )
    table = tf.lookup.StaticVocabularyTable(init, num_oov_buckets=len(vocab))

    
    indices = table[labels]
    labels_onhot = tf.one_hot(indices, n_classes)
    print(image.shape, labels.shape)
    return image, labels_onhot

def count_tfrecord_examples(tfrecord_path):
    """
    Counts the total number of examples in a collection of TFRecord files.

    :param tfrecords_dir: directory that is assumed to contain only TFRecord files
    :return: the total number of examples in the collection of TFRecord files
        found in the specified directory
    """
    count = 0
    count += sum(1 for _ in tf.data.TFRecordDataset(tfrecord_path))

    return count

def _parse_image(example_proto):
    # Parse the input tf.train.Example proto using the dictionary above.
    image_feature_description = {
        'height': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'lbl': tf.io.FixedLenFeature([], tf.int64),
        'img': tf.io.FixedLenFeature([], tf.string),
    }
    return tf.io.parse_single_example(example_proto, image_feature_description)

def _decode_image(example):
    example['img'] = tf.io.parse_tensor(example['img'], out_type=tf.float32)
    example['lbl'] = tf.cast(example['lbl'], tf.int64)
    labels = tf.cast(example['lbl'], tf.int64)
    init = tf.lookup.KeyValueTensorInitializer(
        keys=tf.constant(vocab, dtype=tf.int64), 
        values=tf.constant(list(range(len(vocab))), dtype=tf.int64)
    )
    table = tf.lookup.StaticVocabularyTable(init, num_oov_buckets=len(vocab))  
    indices = table[labels]
    labels_onhot = tf.one_hot(indices, n_classes)
    
    return example['img'], labels_onhot

def _decode_image_(example):
    example['img'] = tf.io.parse_tensor(example['img'], out_type=tf.float32)
    example['lbl'] = tf.cast(example['lbl'], tf.int64)

    return example['img'], example['lbl']

def _data_normalize(image, label):
    min_value = -1024
    max_value = -150
    # Scaling
    image= tf.clip_by_value(image, min_value, max_value)
#     image = tf.image.per_image_standardization(image)  # stdandrize
    image = tf.math.subtract(image, min_value) / (max_value - min_value) #normalize
    return image, label

def _data_augment(image, label):
    # data augmentation. Thanks to the dataset.prefetch(AUTO) statement in the next function (below),
    # this happens essentially for free on TPU. Data pipeline code is executed on the "CPU" part
    # of the TPU while the TPU itself is computing gradients.
    image = tf.image.random_flip_left_right(image)  # flip
    image = tf.image.rot90(image)  # rotation

    return image, label

def load_dataset(out_ffname, train):

    print(out_ffname)
    dataset = tf.data.TFRecordDataset(out_ffname)

    dataset = dataset.map(_parse_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_decode_image_, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    return dataset

def load_batch_dataset(epochs, batch_size, out_ffname, train):
    dataset = tf.data.TFRecordDataset(out_ffname)

    dataset = dataset.map(_parse_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_decode_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    if train:
        dataset = dataset.map(_data_augment, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_data_normalize, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.prefetch(epochs)
    dataset = dataset.repeat(epochs)
    dataset = dataset.shuffle(buffer_size=epochs * batch_size)
    dataset = dataset.batch(batch_size, drop_remainder=True)
    return dataset

def class_weight(dataset, n_classes):
#      n_samples / (n_classes * np.bincount(y))
    labels, counts = np.unique(np.fromiter(dataset.map(lambda x, y: y), np.int32), return_counts=True)
    w = sum(counts) / (n_classes * counts)
    return w

# load train data
train_dataset = load_batch_dataset(epoch, batch_size, out_train_ffname, train=False)
print(f'nm of training samples: {count_tfrecord_examples(out_train_ffname)}')
# load val data
val_dataset = load_batch_dataset(epoch, batch_size, out_val_ffname, train=False)
print(f'nm of training samples: {count_tfrecord_examples(out_val_ffname)}')

# t_dataset = load_dataset(out_train_ffname, train=True)
# weight = class_weight(t_dataset, n_classes)
# weight = {i: weight[i] for i in range(n_classes)}
# print('class weights are:', weight)

# build model
resnet = Resnet2DBuilder.build_resnet_10((n_rows, n_cols, n_channel), n_classes)
resnet.summary()

#metrics
METRICS = [
    tf.keras.metrics.CategoricalAccuracy(name='accuracy'),
#     tf.keras.metrics.Accuracy(name='accuracy'),
    tf.keras.metrics.Precision(name='precision'),
    tf.keras.metrics.Recall(name='recall'),
#     tf.keras.metrics.AUC(name='auc'),
#     tf.keras.metrics.AUC(name='prc', curve='PR'),  # precision-recall curve
]
#compile model
# opt = tf.keras.optimizers.RMSprop(learning_rate=lr)
opt = tf.keras.optimizers.Adam(learning_rate=lr)
resnet.compile(optimizer=opt,
               loss=tf.keras.losses.CategoricalCrossentropy(),
               metrics=METRICS)
#history
history = resnet.fit(
    train_dataset,
    validation_data=val_dataset,
    epochs=epoch,
    steps_per_epoch = int( np.ceil(count_tfrecord_examples(out_train_ffname) / batch_size)),
    validation_steps = int( np.ceil(count_tfrecord_examples(out_val_ffname) / batch_size)),
#     class_weight=weight,
    callbacks=[
     tf.keras.callbacks.TensorBoard(
         log_dir='logs',
         write_graph=True
     ),
     tf.keras.callbacks.ModelCheckpoint(
         mode='max',
         filepath='../checkpoints/resnet10-2D-3cls-hcr_checkpoint_best.h5',
         monitor='val_accuracy',
         save_best_only='True',
         save_weights_only='True',
         verbose=1
     )
    ]
)

#save model
print('train completed!')
np.save('../checkpoints/history10-2D-3cls-hcr.npy', history.history)
resnet.save_weights(os.path.join(w_path, 'resnet10-2D-3cls-hcr.h5'), overwrite=True)


print('save completed!')





