from setuptools import find_packages
from setuptools import setup

with open("VERSION") as version_file:
    VERSION = version_file.read().strip()

if __name__ == "__main__":
    setup(
        name="deeplta",
        version=VERSION,
        description="Deep LTA Model",
        author="Sahar Zafari",
        author_email="saharzafari@imbio.com",
        url="http://imbio.com/",
        license="Private",
        packages=find_packages(),
        test_suite="deeplta.tests",
    )
