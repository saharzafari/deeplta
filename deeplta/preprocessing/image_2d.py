import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


def crop_patch_by_center(img, idx, h, w, z=1):
    """Crop patch (h, w, z) form img centerd at idx.
    
    img:
        Image to crop from
    idx:
        Center of the region to crop
    h:
        height
    
    """
    h_st = idx[1] - h//2
    h_end = idx[1] + h//2

    w_st = idx[2] - w//2
    w_end = idx[2] + w//2

    if z > 1:
        z_st = idx[0] - z//2
        z_end = idx[0] + z//2
        img_cropped = img[z_st:z_end+1, h_st:h_end+1, w_st: w_end+1]
    else:
        img_cropped = img[idx[0], h_st:h_end+1, w_st: w_end+1]
    return img_cropped


def clip(x, min_value=-1200, max_value=-150):
    x = tf.clip_by_value(
        x,
        min_value,
        max_value
    )
    return x


def normalize(x, method='min-max', dtype=np.float32, min_value=None, max_value=None):

    if method == 'min-max':
        if not min_value:
            min_value = tf.reduce_min(x)
        if not max_value:
            max_value = tf.reduce_max(x)
        x = tf.math.subtract(x, min_value) / \
            (max_value - min_value)
        x = x.numpy().astype(dtype)
    else:
        raise ValueError('Invalid `method` to normalize! Valid values are `min-max`.')
    return x


def recover_image(sliced_images, image_size, n_images):
    sliced_images = tf.squeeze(sliced_images)
    kernel_size = sliced_images.shape[1:3]
    steps = [(image_size[i] - kernel_size[i]) // (n_images[i] - 1) for i in range(2)]
    # Initialize image
    recovered = tf.zeros(image_size)
    mask = tf.zeros(image_size, dtype=tf.int8)
    # Kernel for counting overlaps
    kernel_ones = tf.ones(kernel_size,dtype=tf.int8)
    for j in range(n_images[0]):
        for i in range(n_images[1]):
            indexes = tf.meshgrid(tf.range(steps[0] * j, # row start
                                           kernel_size[0] + steps[0] * j), # row end
                                  tf.range(steps[1] * i, # col_start
                                        kernel_size[1] + steps[1] * i), indexing='ij') # col_end
            indexes = tf.stack(indexes, axis=-1)
            recovered = tf.tensor_scatter_nd_add(recovered, indexes, sliced_images[i+j*n_images[0]])
    return recovered 


def extract_2d_patch(images, n_rows, n_cols, strides):
    sizes = [1, n_rows, n_cols, 1]
    strides = [1, strides, strides, 1]
    images = np.expand_dims(images, [-1])
    timages = tf.convert_to_tensor(images, dtype='float32')
    tpatches = tf.image.extract_patches(timages,
                                        sizes=sizes,
                                        strides=strides,
                                        rates=[1, 1, 1, 1],
                                        padding='SAME')
    patch = tf.reshape(tpatches, (-1, n_rows, n_cols, 1))
    return patch


def extract_2d_patch_labels(tlbl_patches, n_rows, n_cols, ratio_th, method='cr'):
    if method == 'max':
        tpatch_labels = tf.reduce_max(tlbl_patches, axis=[1, 2, 3])
    elif method == 'ratio':
        ratio_ann_voxels = tf.math.count_nonzero(tlbl_patches, axis=[1, 2, 3]) / (n_rows * n_cols)
        mask = ratio_ann_voxels < ratio_th
        tpatch_labels = tf.reduce_max(tlbl_patches, axis=[1, 2, 3])
        tpatch_labels = tf.where(mask, tf.zeros(tf.shape(tpatch_labels)), tpatch_labels)
    elif method == 'center':
        center_pixel_ann = tlbl_patches[:, n_rows//2+1, n_cols//2+1, 0]
        mask = center_pixel_ann == 0
        tpatch_labels = tf.reduce_max(tlbl_patches, axis=[1, 2, 3])
        tpatch_labels = tf.where(mask, tf.zeros(tf.shape(tpatch_labels)), tpatch_labels)
    elif method == 'cr':
        center_pixel_ann = tlbl_patches[:, n_rows//2+1, n_cols//2+1, 0]
        mask_voxel_is_annotated = center_pixel_ann != 0
        ratio_ann_voxels = tf.math.count_nonzero(tlbl_patches, axis=[1, 2, 3]) / (n_rows * n_cols)
        mask_ratio_greater_than_threshold = ratio_ann_voxels > ratio_th
        mask_ratio_greater_than_threshold.numpy().sum()
        mask = mask_voxel_is_annotated & mask_ratio_greater_than_threshold
        tpatch_labels = tf.reduce_max(tlbl_patches, axis=[1, 2, 3])
        tpatch_labels = tf.where(mask, tpatch_labels,  tf.zeros(tf.shape(tpatch_labels)))
    else:
        raise ValueError('Invalid label extraction method!')

    return tpatch_labels


def extract_annotated_2d_patch(timg_patches, tlbl_patches, n_rows, n_cols, ratio_th, method):
    # get patch labels
    tpatch_labels = extract_2d_patch_labels(tlbl_patches, n_rows, n_cols, ratio_th, method)
    mask = tpatch_labels > 0
    tlbl_patches_with_annotation = tf.boolean_mask(tlbl_patches, mask, axis=0)
    tpatch_img_with_annotation = tf.boolean_mask(timg_patches, mask, axis=0)
    tpatch_lbl_with_annotation = tf.boolean_mask(tpatch_labels, mask, axis=0)
    return tpatch_img_with_annotation, tlbl_patches_with_annotation, tpatch_lbl_with_annotation


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def show_batch(image_batch, lbl_batch):
    plt.figure(figsize=(50, 50))
    for n in range(image_batch.shape[0]):
        ax = plt.subplot(10, 8, n + 1)
        plt.imshow(image_batch[n], cmap='gray')
        plt.imshow(lbl_batch[n], alpha=0.4)
        plt.axis("off")


def show_tf_batch(image_batch, lbl_batch, vis):
    plt.figure(figsize=(20, 20))
    for n in range(image_batch.shape[0]):
        ax = plt.subplot(6, 10, n + 1)
        plt.imshow(image_batch[n], cmap='gray')
        if vis:
            plt.imshow(lbl_batch[n], alpha=0.4)
        plt.axis("off")


def _parse_image_function(example_proto, vocab, n_rows, n_cols, n_classes):
    image_feature_description = {
        'height': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'lbl': tf.io.FixedLenFeature([], tf.int64),
        'img': tf.io.FixedLenFeature([], tf.string),
    }
    features = tf.io.parse_single_example(example_proto, image_feature_description)
    image = tf.io.parse_tensor(features['img'], out_type=tf.float32)
    image = tf.io.decode_raw(features['img'], tf.float32)
    image.set_shape([n_rows * n_cols])
    image = tf.reshape(image, [n_rows, n_cols])

    labels = tf.cast(features['lbl'], tf.int64)
    init = tf.lookup.KeyValueTensorInitializer(
        keys=tf.constant(vocab, dtype=tf.int64),
        values=tf.constant(list(range(len(vocab))), dtype=tf.int64)
    )
    table = tf.lookup.StaticVocabularyTable(init, num_oov_buckets=len(vocab))
    indices = table[labels]
    labels_onhot = tf.one_hot(indices, n_classes)
    print(image.shape, labels.shape)
    return image, labels_onhot


def count_tfrecord_examples(tfrecord_path):
    count = 0
    count += sum(1 for _ in tf.data.TFRecordDataset(tfrecord_path))
    return count


def _parse_image(example_proto):
    # Parse the input tf.train.Example proto using the dictionary above.
    image_feature_description = {
        'height': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'lbl': tf.io.FixedLenFeature([], tf.int64),
        'img': tf.io.FixedLenFeature([], tf.string),
    }
    return tf.io.parse_single_example(example_proto, image_feature_description)


def _parse_image_test(example_proto):
    # Parse the input tf.train.Example proto using the dictionary above.
    image_feature_description = {
        'img': tf.io.FixedLenFeature([], tf.string),
        'idx': tf.io.FixedLenFeature([], tf.string),
    }
    return tf.io.parse_single_example(example_proto, image_feature_description)


def _decode_image_(example):
    example['img'] = tf.io.parse_tensor(example['img'], out_type=tf.float32)
    example['lbl'] = tf.cast(example['lbl'], tf.int64)
    return example['img'], example['lbl']


def _decode_image_test(example):
    example['img'] = tf.io.parse_tensor(example['img'], out_type=tf.float32)
    example['idx'] = tf.io.parse_tensor(example['idx'], out_type=tf.int16)
    return example['img'], example['idx']


def _data_normalize(image, label, clip_values=False):
#     min_value = -1200
#     max_value = -150
    min_value = -1450
    max_value = -50
    image = tf.clip_by_value(image, min_value, max_value)
    image = tf.math.subtract(image, min_value) / (max_value - min_value) 
#     image = tf.clip_by_value(image, 0, 1)
    # Scaling
#     image = tf.image.per_image_standardization(image)  # stdandrize
    return image, label


def _data_normalize_md(image, label):

#      min_values = [-1400,-1400, -160]
#     max_values = [-950,200, 240]
    min_values = [-1200]
    max_values = [-150]
    clip_value_min = tf.constant(min_values, dtype=tf.float32)
    clip_value_max = tf.constant(max_values, dtype=tf.float32)
    # expand to channel dim
#     image_md = tf.expand_dims(image, -1)
#     image = median_filter2d(image,(3,3), 'CONSTANT', 0) #median filter
    
    image_md = tf.tile(image, tf.constant([1, 1, len(min_values)]))
    image_md = tf.clip_by_value(image_md, clip_value_min, clip_value_max)
    image_md = tf.math.subtract(image_md, clip_value_min) / (clip_value_max - clip_value_min) 

    return image_md, label


def _data_augment(image, label):
    #     image = tf.image.per_image_standardization(image)  # normalization
    image = tf.image.random_flip_left_right(image)  # flip
    image = tf.image.rot90(image)  # rotation
    return image, label


def load_test_dataset(fname):
    print(fname)
    dataset = tf.data.TFRecordDataset(fname)
    dataset = dataset.map(_parse_image_test, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_decode_image_test, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    return dataset


def load_dataset(fname):
    print(fname)
    dataset = tf.data.TFRecordDataset(fname)
    dataset = dataset.map(_parse_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_decode_image_, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    return dataset


def load_batch_dataset(epochs, batch_size, fname, vocab, n_classes, aug, train, normalize):

    def _decode_image(example):
        example['img'] = tf.io.parse_tensor(example['img'], out_type=tf.float32)
        example['lbl'] = tf.cast(example['lbl'], tf.int64)
        labels = tf.cast(example['lbl'], tf.int64)
        init = tf.lookup.KeyValueTensorInitializer(
            keys=tf.constant(vocab, dtype=tf.int64),
            values=tf.constant(list(range(len(vocab))), dtype=tf.int64)
        )
        table = tf.lookup.StaticVocabularyTable(init, num_oov_buckets=len(vocab))
        indices = table[labels]
        labels_onehot = tf.one_hot(indices, n_classes)
        return example['img'], labels_onehot

    dataset = tf.data.TFRecordDataset(fname)
    dataset = dataset.map(_parse_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_decode_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    if aug:
        dataset = dataset.map(_data_augment, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    if normalize:
        dataset = dataset.map(_data_normalize_md, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.prefetch(epochs)
    if train:
        dataset = dataset.repeat(epochs)
        dataset = dataset.shuffle(buffer_size=epochs * batch_size)
        dataset = dataset.batch(batch_size, drop_remainder=True)
    else:
        dataset = dataset.batch(batch_size, drop_remainder=False)
    return dataset


def load_train_val_dataset(epochs, batch_size, out_ffname, vocab, n_classes, train):
    
    def _decode_image(example):
        example['img'] = tf.io.parse_tensor(example['img'], out_type=tf.float32)
        example['lbl'] = tf.cast(example['lbl'], tf.int64)
        labels = tf.cast(example['lbl'], tf.int64)
        init = tf.lookup.KeyValueTensorInitializer(
            keys=tf.constant(vocab, dtype=tf.int64),
            values=tf.constant(list(range(len(vocab))), dtype=tf.int64)
        )
        table = tf.lookup.StaticVocabularyTable(init, num_oov_buckets=len(vocab))
        indices = table[labels]
        labels_onehot = tf.one_hot(indices, n_classes)
        return example['img'], labels_onehot
    
    dataset = tf.data.TFRecordDataset(out_ffname)
    dataset = dataset.map(_parse_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_decode_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    if train:
        dataset = dataset.map(_data_augment, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.map(_data_normalize_md, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.prefetch(epochs)
    dataset = dataset.repeat(epochs)
    dataset = dataset.shuffle(buffer_size=epochs * batch_size)
    dataset = dataset.batch(batch_size, drop_remainder=True)
    train_size = int(0.9 * count_tfrecord_examples(out_ffname))
    train_dataset = dataset.take(train_size)
    val_size = int(0.1 * count_tfrecord_examples(out_ffname))
    val_dataset = dataset.skip(val_size)
    return train_dataset, val_dataset, train_size, val_size


def class_weight(dataset, n_classes):
    #      n_samples / (n_classes * np.bincount(y))
    labels, counts = np.unique(np.fromiter(dataset.map(lambda x, y: y), np.int32), return_counts=True)
    w = sum(counts) / (n_classes * counts)
    return w
