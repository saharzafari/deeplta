
import json
from types import SimpleNamespace


class Config(object):
    """Config class which contains data, train, predict and model hyper-parameters"""

    def __init__(self, data, model, train=None, predict=None, seed=None):
        self.data = data
        self.train = train
        self.predict = predict
        self.model = model
        self.seed = seed

    @classmethod
    def from_json(cls, path):
        """Creates config from json"""
        with open(path, 'r') as fo:
            params = json.load(fo)
        return cls(**params)
    @classmethod
    def from_default(cls):
        data = {
            "clip_min_value": -1200,
            "clip_max_value": -150
        }
        model = {
            "path": "/home/katiecarey/src/deeplta/deeplta/checkpoints/deeplta_resnet_classifier.h5",
            "input_shape": [32, 32, 1],
            "n_classes": 6,
            "patch_size": [32, 32]
        }
        predict = {"batch_size": 1024}
        seed = 2020
        return cls(data,model,predict=predict,seed=seed)
