import os
from abc import ABC
from abc import abstractmethod
import numpy as np
import tensorflow as tf
from medpy.io import load as medpy_load
from dicomhd import io as dhd_io


class DicomImage(object):

    def __init__(self, path):
        self.path = path
        self._data = self.load(self.path)

    def load(self):
        dcm_series = dhd_io.read_series(self.path)
        data = dcm_series.pixel_data
        return data



class BaseDicomDataset(ABC):

    def __init__(self, path, class_map):
        self.path = path
        self.class_map = class_map

    @abstractmethod
    def load(
            self,
            path: str,
            epochs: int,
            batch_size: int = None,
            augment: bool = None,
            shuffle: bool = False,
            repeat: bool = False,
            drop_remainder: bool = False
    ):
        pass

    @abstractmethod
    def load_train(self):
        pass

    @abstractmethod
    def load_valid(self):
        pass

    @abstractmethod
    def load_test(self):
        pass

    def extract_2d_patch(
            self,
            size: int,
            strides: int,
            image_path: str,
            label_path: str,
            out_path: str,
            method: str = 'center',
            ratio_th: float = 0.5,
            dataset_type: str = 'tfrecords',
            device: str = '/cpu:0',
    ):
        rows, cols = size
        with tf.device(device):

            img_file_names = sorted([f for f in os.listdir(image_path)])
            lbl_file_names = sorted([f for f in os.listdir(label_path)])

            if dataset_type == 'tfrecords':
                n_samples = 0
                writer = tf.io.TFRecordWriter(out_path)
                for img_file_name, lbl_file_name in zip(img_file_names, lbl_file_names):
                    if '.DS_Store' in img_file_name or '.DS_Store' in lbl_file_name:
                        continue
                    print(img_file_name, ',', lbl_file_name)
                    # extract label patches
                    lbl_file_path = os.path.join(label_path, lbl_file_name)
                    lbl_file_path = os.path.join(lbl_file_path, 'annotation.gipl')
                    lbl, label_header = medpy_load(lbl_file_path)
                    lbl = np.swapaxes(lbl, 0, 2)
                    if lbl.sum() > 0:
                        # get indices for voxels with ann
                        idx_with_ann = np.ma.where(lbl.sum(axis=(1, 2)) > 0)[0]
                        # crop label
                        lbl_ann = lbl[idx_with_ann, ...]
                        print('label is:', lbl_ann.max())
                        if 'groundglass' in img_file_name and lbl_ann.max() == 1:
                            mask_gg = lbl_ann == 1
                            lbl_ann[mask_gg] = 3
                        tlbl_patches = self.extract_2d_patch_numpy(lbl_ann, size, strides)
                        # load image
                        img_file_path = os.path.join(image_path, img_file_name)
                        img = dhd_io.read_series(img_file_path)
                        # crop image
                        img_ann = img.pixel_data[idx_with_ann, ...]
                        # extract image patches
                        img_patches = self.extract_2d_patch_numpy(img_ann, size, strides)
                        # extract patch with annotation
                        img_patches_with_ann, lbl_patches_with_ann, patch_lbls_with_ann = \
                            BaseDicomDataset.extract_annotated_2d_patch(
                                img_patches,
                                tlbl_patches,
                                size,
                                ratio_th=ratio_th,
                                method=method
                            )

                        for patch_idx in range(img_patches_with_ann.shape[0]):
                            img_patch_idx = img_patches_with_ann[patch_idx, ...]
                            # tlbl_patch_idx = tlbl_patches_with_ann[patch_idx, ...]
                            patch_lbl_idx = patch_lbls_with_ann[patch_idx, ...]
                            example = tf.train.Example(
                                features=tf.train.Features(feature={
                                    'height': tf.train.Feature(int64_list=tf.train.Int64List(
                                        value=[rows])),
                                    'width': tf.train.Feature(int64_list=tf.train.Int64List(
                                        value=[cols])),
                                    'lbl': tf.train.Feature(int64_list=tf.train.Int64List(
                                        value=[int(patch_lbl_idx.numpy())])),
                                    'img': tf.train.Feature(bytes_list=tf.train.BytesList(
                                        value=[tf.io.serialize_tensor(img_patch_idx).numpy()])),
                                })
                            )
                            writer.write(example.SerializeToString())

                    n_samples += img_patches_with_ann.shape[0]
                    print(f'patch dim: {img_patches_with_ann.shape}, patch lbl dim: {lbl_patches_with_ann.shape}')
                    print('-' * 50)

                writer.close()

    @staticmethod
    def extract_2d_patch_numpy(image, size, strides):
        n_rows, n_cols = size
        sizes = [1, n_rows, n_cols, 1]
        strides = [1, strides, strides, 1]
        image = np.expand_dims(image, [-1])
        timages = tf.convert_to_tensor(image, dtype='float32')
        tpatches = tf.image.extract_patches(timages,
                                            sizes=sizes,
                                            strides=strides,
                                            rates=[1, 1, 1, 1],
                                            padding='SAME')
        patch = tf.reshape(tpatches, (-1, n_rows, n_cols, 1))
        return patch

    @staticmethod
    def extract_2d_patch_labels(lbl_patches_t, size, ratio_th, method='center-ratio'):
        rows, cols = size
        if method == 'max':
            patch_labels_t = tf.reduce_max(lbl_patches_t, axis=[1, 2, 3])
        elif method == 'ratio':
            ratio_ann_voxels = tf.math.count_nonzero(lbl_patches_t, axis=[1, 2, 3]) / (rows * cols)
            mask = ratio_ann_voxels < ratio_th
            patch_labels_t = tf.reduce_max(lbl_patches_t, axis=[1, 2, 3])
            patch_labels_t = tf.where(mask, tf.zeros(tf.shape(patch_labels_t)), patch_labels_t)
        elif method == 'center':
            center_pixel_ann = lbl_patches_t[:, rows // 2 + 1, cols // 2 + 1, 0]
            mask = center_pixel_ann == 0
            patch_labels_t = tf.reduce_max(lbl_patches_t, axis=[1, 2, 3])
            patch_labels_t = tf.where(mask, tf.zeros(tf.shape(patch_labels_t)), patch_labels_t)
        elif method == 'center-ratio':
            center_pixel_ann = lbl_patches_t[:, rows // 2 + 1, cols // 2 + 1, 0]
            mask_voxel_is_annotated = center_pixel_ann != 0
            ratio_ann_voxels = tf.math.count_nonzero(lbl_patches_t, axis=[1, 2, 3]) / (rows * cols)
            mask_ratio_greater_than_threshold = ratio_ann_voxels > ratio_th
            mask_ratio_greater_than_threshold.numpy().sum()
            mask = mask_voxel_is_annotated & mask_ratio_greater_than_threshold
            patch_labels_t = tf.reduce_max(lbl_patches_t, axis=[1, 2, 3])
            patch_labels_t = tf.where(mask, patch_labels_t, tf.zeros(tf.shape(patch_labels_t)))
        else:
            raise ValueError('Invalid label extraction method!')

        return patch_labels_t

    @staticmethod
    def extract_annotated_2d_patch(img_patches_t, lbl_patches_t, size, ratio_th=0.5, method='center'):
        # get patch labels
        patch_labels_t = BaseDicomDataset.extract_2d_patch_labels(lbl_patches_t, size, ratio_th, method)
        mask = patch_labels_t > 0
        lbl_patches_with_annotation_t = tf.boolean_mask(lbl_patches_t, mask, axis=0)
        patch_img_with_annotation_t = tf.boolean_mask(img_patches_t, mask, axis=0)
        patch_lbl_with_annotation_t = tf.boolean_mask(patch_labels_t, mask, axis=0)
        return patch_img_with_annotation_t, lbl_patches_with_annotation_t, patch_lbl_with_annotation_t


class TFRecordDicomDataset(BaseDicomDataset):

    def __init__(self, path: str, features: dict, class_map: list = None):

        super(TFRecordDicomDataset, self).__init__(path, class_map)
        self.features = features

    def load(
            self,
            path: str,
            epochs: int,
            batch_size: int = None,
            augment: bool = False,
            shuffle: bool = False,
            repeat: bool = False,
            drop_remainder: bool = False
    ):
        for feature, prop in self.features.items():
            if prop.get('onehot', {}):
                print(prop)
                init = tf.lookup.KeyValueTensorInitializer(
                    keys=tf.constant(prop['onehot']['class_map'], dtype=tf.int64),
                    values=tf.constant(list(range(len(prop['onehot']['class_map']))), dtype=tf.int64)
                )
                index_map = tf.lookup.StaticVocabularyTable(init, num_oov_buckets=len(prop['onehot']['class_map']))
                prop['onehot']['index_map'] = index_map

        def _decode_image(example):

            for feature, prop in self.features.items():
                if prop.get('dtype', None):
                    if example[feature].dtype == tf.string:
                        example[feature] = tf.io.parse_tensor(example[feature], getattr(tf, prop['dtype']))
                    else:
                        example[feature] = tf.cast(example[feature], getattr(tf, prop['dtype']))
                if prop.get('onehot', {}):
                    indices = prop['onehot']['index_map'][example[feature]]
                    example[feature] = tf.one_hot(indices, len(prop['onehot']['class_map']))

            return [example[feature] for feature in self.features.keys()]

        dataset = tf.data.TFRecordDataset(path)
        dataset = dataset.map(self._parse_example, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        dataset = dataset.map(_decode_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        # if aug:
        #     dataset = dataset.map(_data_augment, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        # if normalize:
        #     dataset = dataset.map(_data_normalize_md, num_parallel_calls=tf.data.experimental.AUTOTUNE)
        # dataset = dataset.prefetch(epochs)
        if shuffle:
            dataset = dataset.repeat(epochs)
        if repeat:
            dataset = dataset.shuffle(buffer_size=epochs * batch_size)
        if batch_size:
            dataset = dataset.batch(batch_size, drop_remainder=drop_remainder)
        return dataset

    def load_train(self):
        pass

    def load_valid(self):
        pass

    def load_test(self):
        pass

    def _parse_example(self, sample):
        features = {
            'height': tf.io.FixedLenFeature([], tf.int64),
            'width': tf.io.FixedLenFeature([], tf.int64),
            'lbl': tf.io.FixedLenFeature([], tf.int64),
            'img': tf.io.FixedLenFeature([], tf.string),
        }
        return tf.io.parse_single_example(sample, features)
