import boto3
import botocore
import hashlib


def calc_file_etag(file_path: str, chunk_size: str = 8 * 1024 * 1024):
    md5s = []

    with open(file_path, 'rb') as fp:
        while True:
            data = fp.read(chunk_size)
            if not data:
                break
            md5s.append(hashlib.md5(data))
    if len(md5s) < 1:
        return '"{}"'.format(hashlib.md5().hexdigest())
    if len(md5s) == 1:
        return '"{}"'.format(md5s[0].hexdigest())
    digests = b''.join(m.digest() for m in md5s)
    digests_md5 = hashlib.md5(digests)
    return '{}-{}'.format(digests_md5.hexdigest(), len(md5s))


def get_s3_md5sum(bucket_name: str, object_name: str):
    try:
        md5sum = boto3.client('s3').head_object(
            Bucket=bucket_name,
            Key=object_name
        )['ETag'][1:-1]
    except botocore.exceptions.ClientError as e:
        raise RuntimeError(e)
    return md5sum


def verify_md5sum(bucket_name: str, object_name: str, file_name: str, chunk_size: int) -> True:
    md5sum_s3 = get_s3_md5sum(bucket_name, object_name)
    md5sum_loc = calc_file_etag(file_name, chunk_size)
    return True if md5sum_loc == md5sum_s3 else False