# ==============================================================================
# Copyright (c) Imbio. All rights reserved.
#
# definitions.py - Globally used constants and definitions are placed here
# ==============================================================================
import json
import logging
import os
from enum import Enum

import numpy as np

ALG_VERSION = os.environ["IMBIO_LDA_VERSION"]
ASSETS_PATH = os.path.join(os.path.dirname(__file__), "assets")
MEDIA_PATH = os.path.join(ASSETS_PATH, "reporting")
CONFIG_PATH = os.path.join(ASSETS_PATH, "config")
DL_PATH = os.path.join(ASSETS_PATH, "deep_learning")
DEFAULT_DICOM_FILENAME = "00000001.dcm"
DEFAULT_SET_CONFIG_FILE = "default_set_config.json"  # file in CONFIG_PATH

# Registration settings files
# REGISTRATION_SETTINGS_FILE = os.path.join(CONFIG_PATH, "registration_settings.json")

# with open(REGISTRATION_SETTINGS_FILE) as fp:
#     REGISTRATION_SETTINGS = json.load(fp)

# Deep learning settings files
FISSURE_SEGMENTATION_SETTINGS_FILE = os.path.join(
    DL_PATH, "fissure_segmentation_settings.json"
)
LUNG_MASKING_SETTINGS_FILE = os.path.join(DL_PATH, "lung_masking_settings.json")
LOBE_SEGMENTATION_SETTINGS_FILE = os.path.join(
    DL_PATH, "lobe_segmentation_settings.json"
)

# Deep learning model files
FISSURE_SEGMENTATION_MODEL_FILE = os.path.join(
    DL_PATH, "fissure_segmentation_model.pkl"
)
LUNG_MASKING_MODEL_FILE = os.path.join(DL_PATH, "lung_masking_model.pkl")
LOBE_SEGMENTATION_MODEL_FILE = os.path.join(DL_PATH, "%s_lobe_segmentation_model.pkl")

MINIMUM_LOBAR_VOLUME = 0.1  # in liters
DEFAULT_JITTER_RADIUS = (
    15.0 / 2.0
)  # 15 mm - As specified in Design Verification Test STP-ALG-021
DEFAULT_JITTER_STEPS = 2

SEGMENTATION_PADDING_MM = (20.0, 10.0, 10.0)

# These are in Liters
MAX_ACCEPTABLE_LUNG_VOL = 13.0
MIN_ACCEPTABLE_LUNG_VOL = 1.0

MAX_ACCEPTABLE_AIRWAY_VOL = 0.1
MIN_ACCEPTABLE_AIRWAY_VOL = 0.005

# Input check stuff
# For more details on this, see the docstring for image_processing.get_most_common_slice_spacing
MIN_PERCENTAGE_FOR_COMMON_SLICE_SPACING = 0.95
MIN_PERCENTAGE_FOR_COMMON_LUNG_SLICE_SPACING = 0.99

# Acceptable slice spacing thresholds
SS_THRESH_SELECT = 1.5
SS_THRESH_DEFAULT = 2.5


# Hole-filling threshold. When filling holes int he segmentation, don't fill in areas greater than this value
HOLE_FILLING_THRESHOLD = -250

###     Fissure completeness params
###################################
###################################
""" This is the threshold use to determine that fissure completeness
"""
BENDING_ENERGY_THRESHOLD_ERROR = 2500.0
BENDING_ENERGY_THRESHOLD_WARNING = 1000.0

"""
This is used for generating a sampling mask near the lobe boundaries
The sampling mask is where fissures are searched for when finding the thin-plate spline params.
Anything outside of the sampling mask is assumed to not contain the fissure
"""
TPS_FISSURE_BUFFER_DILATION_ITERATIONS = 5

"""Thin-plate spline smoothness coefficient. This is determined empirically
"""
TPS_SMOOTHNESS = 100

"""Number of control points for estimating the spline
"""
N_CONTROL_POINTS = 15000

"""Number of interpolation points for estimating the spline
"""
N_INTERP_POINTS = 25000

"""
This value shjould be between 1 and 0. It is the threshold for converting the
probability map to a binary classification map, and can be used to change
sensitivity/specificity
"""
FISSURE_PROBABILITY_BINARY_THRESHOLD_CLASSIFICATION = 0.60
FISSURE_PROBABILITY_BINARY_THRESHOLD_CLASSIFICATION_SENSITIVE = 0.05
FISSURE_PROBABILITY_BINARY_THRESHOLD_CLASSIFICATION_SPECIFIC = 0.95


# This is a more sensitive threshhold use for segmentation cleanup but not fissure completeness
SENSITIVE_FISSURE_PROBABILITY_BINARY_THRESHOLD_CLASSIFICATION = 0.025

"""This controls how much fissures near the edges of the lung are ignored.
Fissures near the edge of the lung are difficult to segment and detect and may not relaible
Increaing the value effectively increases the border region near the edge of
the lung where they are ignored.
CANNOT USE 0
"""
FISSURE_LUNG_BORDER_EROSION_ITERATIONS = 3


"""Fissure competeness dilation size. This is applied to the binary fissure map
If you want to increase the overall sensitivity of the fissure completeness computation step,
increase the value of this parameter
"""
FISSURE_COMPLETENESS_DILATION_ITERATIONS = 1


""" Intensity threshold use to create an intensity mask
the intensity mask is combined with the fissure completeness mask

"""
FISSURE_COMPLETENESS_INTENSITY_THRESHOLD = -250

"""
This is used for generating a sampling mask near the lobe boundaries
The sampling mask is where fissures are searched for when running the random walker algorithm.
Anything outside of the sampling mask is assumed to not contain the fissure
"""
RW_FISSURE_BUFFER_DILATION_ITERATIONS = 27


"""Random walker beta value. Defines random walker motion
"""
RW_BETA = 100000
RW_FILTERING_SIGMA = 0.707  # 1/sqrt(2), a common value

INCOMPLETE_FISSURE_INDEX = 1
COMPLETE_FISSURE_INDEX = 2

# In order to include scar and fibrotic tissue, which is often left out of the segmentation,
# we need to include tissue using morphological closing
TISSUE_INCLUSION_CLOSING_ITERATIONS = 5

######

MIN_NUM_POSSIBLE_FISSURE_VOXELS = 100

# VESSELNESS_SETTINGS_FILE = os.path.join(CONFIG_PATH, "vessel_filter_settings.json")
# with open(VESSELNESS_SETTINGS_FILE) as fp:
#     vessel_settings = json.load(fp)

# VESSELNESS_ALPHA = vessel_settings["vessels"]["alpha"]
# VESSELNESS_BETA = vessel_settings["vessels"]["beta"]
# VESSELNESS_GAMMA = vessel_settings["vessels"]["gamma"]
# VESSELNESS_MIN_SCALE = vessel_settings["vessels"]["min_scale"]
# VESSELNESS_MAX_SCALE = vessel_settings["vessels"]["max_scale"]
# VESSELNESS_NUM_SCALES = vessel_settings["vessels"]["num_scales"]
# VESSELNESS_THRESHOLD = vessel_settings["vessels"]["t"]
# VESSELNESS_DIM = vessel_settings["vessels"]["D"]

# SCAR_ALPHA = vessel_settings["scar"]["alpha"]
# SCAR_BETA = vessel_settings["scar"]["beta"]
# SCAR_GAMMA = vessel_settings["scar"]["gamma"]
# SCAR_MIN_SCALE = vessel_settings["scar"]["min_scale"]
# SCAR_MAX_SCALE = vessel_settings["scar"]["max_scale"]
# SCAR_NUM_SCALES = vessel_settings["scar"]["num_scales"]
# SCAR_THRESHOLD = vessel_settings["scar"]["t"]
# SCAR_DIM = vessel_settings["scar"]["D"]

# When performing image tranformations with ITK, the default pixel value will
# define the value of pixels outside of the image
ITK_DEFAULT_PIXEL_VALUE = -9999

# For converting nifti sform matrices between LPS and RAS coordinate systems
NIFTI_CONVERSION_MATRIX = np.eye(4).astype(float)
NIFTI_CONVERSION_MATRIX[0, 0] = -1.0
NIFTI_CONVERSION_MATRIX[1, 1] = -1.0

# This is how many times we run the DL model
MASKING_N_PERTURBS = 3
SEGMENTATION_N_PERTURBS = 7


# This is how we check if a lung is segmented or not.
MINIMUM_ACCEPTABLE_LUNG_VOLUME = 300  # in mL

NIFTI_OUTPUT_FOLDER = os.path.join("images", "nifti")
PNG_OUTPUT_FOLDER = os.path.join("images", "png")

SET_OUTPUT_FOLDER = "set"
TEMPDIR = "temp_report_files"  # used for report generation
SEGMENTATION_OUTPUT_FOLDER = "segmentation_output"
DEBUG_OUTPUT_FOLDER = "debug"
REGISTRATION_WORKING_FOLDER = "working"
REGISTRATION_CONFIG_FILE = "config.ini"
WARPED_IMAGE_DIR_NAME = "warped"


# We downsample the image to this resolution
# Remember that it is z,y,x
REGISTRATION_SPACING = (2.0, 1.0, 1.0)
REGISTRATION_SPACING_XYZ = tuple(list(REGISTRATION_SPACING)[::-1])

RAW_SEGMENTATION_DICOM_SERIES_FOLDER = "raw"
RGB_SEGMENTATION_DICOM_SERIES_FOLDER = "rgb"
# This dictionary is used to convert verbosity argument numbers to logging level
LOGGER_LEVELS = {
    0: logging.DEBUG,
    1: logging.INFO,
    2: logging.WARNING,
    3: logging.ERROR,
}

SET_PRIVATE_CREATOR = "SegmentationEditor"
SET_CONFIG_FILENAME = "seg_editor_config.json"  # output SET config file
PRIVATE_CREATOR_VR = "LO"
EDITOR_NAME_VR = "PN"
SET_VERSION_VR = "SH"
SET_RUN_COUNT_VR = "IS"

LANGUAGE_UTILS = {
    "Male": {
        "english": "Male",
        "german": "Männlich",
        "spanish": "Varón",
        "italian": "Maschile",
        "french": "Masculin",
        "portuguese": "Masculino",
    },
    "Female": {
        "english": "Female",
        "german": "Weiblich",
        "spanish": "Mujere",
        "italian": "Femmina",
        "french": "Femme",
        "portuguese": "Feminino",
    },
    "Missing": {
        "english": "Missing",
        "german": "Fehlend",
        "spanish": "Ausente",
        "italian": "Mancante",
        "french": "Manquant",
        "portuguese": "Ausente",
    },
    "Inconsistent": {
        "english": "Inconsistent",
        "german": "Inkonsistent",
        "spanish": "Inconsistente",
        "italian": "Incoerente",
        "french": "Incohérent",
        "portuguese:": "Inconsistente",
    },
    "Inconsistent within Lungs": {
        "english": "Inconsistent within Lungs",
        "german": "Inkonsistent innerhalb der Lunge",
        "spanish": "Inconsistente dentro de los pulmones",
        "italian": "Incoerente nei polmoni",
        "french": "Incohérent au sein des poumons",
        "portuguese": "Inconsistente entre os pulmões",
    },
    "Not Present": {
        "english": "Not Present",
        "german": "Nicht vorhanden",
        "spanish": "No está",
        "italian": "Non presente",
        "french": "Absent",
        "portuguese": "Não está presente",
    },
    "NaN": {
        "english": "NaN",
        "german": "NaN",
        "spanish": "No es número",
        "italian": "NaN",
        "french": "Non numérique",
        "portuguese": "NaN",
    },
    "Spacing": {
        "english": "Spacing",
        "german": "Abstand",
        "spanish": "Espacios",
        "italian": "Distanza",
        "french": "Espacement",
        "portuguese": "Espaçamento",
    },
    "Could not compute": {
        "english": "Could not compute",
        "german": "Berechnung nicht möglich",
        "spanish": "No se pudo calcular",
        "italian": "Impossibile elaborare",
        "french": "Traitement impossible",
        "portuguese": "Não foi possível computar",
    },
    "Unable to Calculate": {
        "english": "Unable to calculate",
        "german": "Ermittlung nicht möglich",
        "spanish": "Imposible calcular",
        "italian": "Impossibile calcolare",
        "french": "Impossible de calculer",
        "portuguese": "Não foi possível calcular",
    },
    "Consistent": {
        "english": "Consistent",
        "german": "Konsistent",
        "spanish": "Consistente",
        "italian": "Coerente",
        "french": "Cohérent",
        "portuguese": "Consistente",
    },
    "Non-edge-enhancing": {
        "english": "Non-edge-enhancing",
        "german": "Keine Kantenschärfung",
        "spanish": "Realce sin borde",
        "italian": "Senza miglioramento dei bordi",
        "french": "Sans renforcement des bords",
        "portuguese": "Aprimoramento não delimitado",
    },
    "Unknown": {
        "english": "Unknown",
        "german": "Unbekannt",
        "spanish": "Desconocido",
        "italian": "Sconosciuto",
        "french": "Inconnu",
        "portuguese": "Desconhecido",
    },
    "N/A": {
        "english": "N/A",
        "german": "N/Z",
        "spanish": "N/D",
        "italian": "N/A",
        "french": "S/O",
        "portuguese": "N/A",
    },
    "Left": {
        "english": "Left",
        "german": "Linker",
        "spanish": "Izquierdo",
        "italian": "Sinistro",
        "french": "Gauche",
        "portuguese": "Esquerdo",
    },
    "Right": {
        "english": "Right",
        "german": "Rechter",
        "spanish": "Derecho",
        "italian": "Destro",
        "french": "Droit",
        "portuguese": "Direito",
    },
    "Upper Right Lung": {
        "english": "Upper Right Lung",
        "german": "Oberer Rechter Lungenflügel",
        "spanish": "Parte Superior del Pulmón Derecho",
        "italian": "Parte Superiore del Polmone Destro",
        "french": "Poumon Supérieur Droit",
        "portuguese": "Pulmão Direito Superior",
    },
    "Middle Right Lung": {
        "english": "Middle Right Lung",
        "german": "Mittlerer Rechter Lungenflügel",
        "spanish": "Parte Central del Pulmón Derecho",
        "italian": "Parte Centrale del Polmone Destro",
        "french": "Poumon intermédiaire droit",
        "portuguese": "Pulmão direito médio",
    },
    "Lower Right Lung": {
        "english": "Lower Right Lung",
        "german": "Unterer Rechter Lungenflügel",
        "spanish": "Parte Inferior del Pulmón Derecho",
        "italian": "Parte Inferiore del Polmone Destro",
        "french": "Poumon Inférieur Droit",
        "portuguese": "Pulmão Direito Inferior",
    },
    "Upper Left Lung": {
        "english": "Upper Left Lung",
        "german": "Oberer Linker Lungenflügel",
        "spanish": "Parte Superior del Pulmón Izquierdo",
        "italian": "Parte Superiore del Polmone Sinistro",
        "french": "Poumon Supérieur Gauche",
        "portuguese": "Pulmão Esquerdo Superior",
    },
    "Middle Left Lung": {
        "english": "Middle Left Lung",
        "german": "Mittlerer Linker Lungenflügel",
        "spanish": "Parte Central del Pulmón Izquierdo",
        "italian": "Parte Centrale del Polmone Sinistro",
        "french": "Poumon Intermédiaire Gauche",
        "portuguese": "Pulmão Esquerdo Médio",
    },
    "Lower Left Lung": {
        "english": "Lower Left Lung",
        "german": "Unterer Linker Lungenflügel",
        "spanish": "Parte Inferior del Pulmón Izquierdo",
        "italian": "Parte Inferiore del Polmone Sinistro",
        "french": "Poumon Inférieur Gauche",
        "portuguese": "Pulmão Esquerdo Inferior",
    },
    "Right_Upper": {
        "english": "Upper Right",
        "german": "Oberer Rechter",
        "spanish": "Parte Superior Derecho",
        "italian": "Parte Superiore Destro",
        "french": "Supérieur Droit",
        "portuguese": "Direito Superior",
    },
    "Right_Middle": {
        "english": "Middle Right",
        "german": "Mittlerer Rechter",
        "spanish": "Parte Central Derecho",
        "italian": "Parte Centrale Destro",
        "french": "Intermédiaire droit",
        "portuguese": "Direito Médio",
    },
    "Right_Lower": {
        "english": "Lower Right",
        "german": "Unterer Rechter",
        "spanish": "Parte Inferior Derecho",
        "italian": "Parte Inferioree Destro",
        "french": "Inférieur Droit",
        "portuguese": "Direito Inferior",
    },
    "Left_Upper": {
        "english": "Upper Left",
        "german": "Oberer Linker",
        "spanish": "Parte Superior Izquierdo",
        "italian": "Parte Superiore Sinistro",
        "french": "Supérieur Gauche",
        "portuguese": "Esquerdo Superior",
    },
    "Left_Middle": {
        "english": "Middle Left",
        "german": "Mittlerer Linker",
        "spanish": "Parte Central Izquierdo",
        "italian": "Parte Centrale Sinistro",
        "french": "Intermédiaire Gauche",
        "portuguese": "Esquerdo Médio",
    },
    "Left_Lower": {
        "english": "Lower Left",
        "german": "Unterer Linker",
        "spanish": "Parte Inferior Izquierdo",
        "italian": "Parte Inferiore Sinistro",
        "french": "Inférieur Gauche",
        "portuguese": "Esquerdo Inferior",
    },
    "no_lobe_warning": {
        "english": "No %s lobe identified in segmentation",
        "german": "Kein %s Lappen in Segmentierung festgestellt",
        "spanish": "No se identificó el lóbulo %s en la segmentación",
        "italian": "Nessun lobo %s identificato nella segmentazione",
        "french": "Aucun lobe %s identifié dans la segmentation",
        "portuguese": "Nenhum lobo %s identificado na segmentação",
    },
    "no_lung_warning": {
        "english": "No %s lung identified in segmentation",
        "german": "Keine %s Lunge in Segmentierung festgestellt",
        "spanish": "No se identificó el pulmón %s en la segmentación",
        "italian": "Nessun polmone %s identificato nella segmentazione",
        "french": "Aucun poumon %s identifié dans la segmentation",
        "portuguese": "Nenhum pulmão %s identificado na segmentação",
    },
}

LANGUAGE_STRING_DICT = {
    "english": "en",
    "german": "de_DE",
    "italian": "it_IT",
    "french": "fr_FR",
    "spanish": "es_ES",
    "portuguese": "pt_BR",
}

LANGUAGE_TAG_DICT = {
    "english": "ENG",
    "german": "DEU",
    "italian": "ITA",
    "spanish": "SPA",
    "french": "FRA",
    "portuguese": "POR",
}

WARNINGS = {
    "input_check_warning": {
        "english": "Input checks were deactivated due to running in research mode. For Investigational Use Only.",
        "german": "Die Eingabeüberprüfungen wurden aufgrund der Ausführung im Forschungsmodus deaktiviert. Ergebnisse nur zu Untersuchungszwecken vorgesehen.",
        "spanish": "Se desactivaron las comprobaciones de entrada debido a la ejecución en el modo de investigación. Resultados solo para uso en investigación.",
        "italian": "I controlli in ingresso sono stati disattivati a causa dell'esecuzione in modalità di ricerca. I risultati sono solo per uso sperimentale.",
        "french": "Les contrôles de saisie ont été désactivés en raison de l'exécution en mode recherche. Résultats exclusivement destinés à la recherche.",
        "portuguese": "As verificações de entrada foram desativadas por estar sendo executado no modo de pesquisa. Apenas para uso investigativo.",
    },
    "edge_kernel_warning": {
        "english": "%s is an edge-enhancing kernel, which is NOT recommended for Imbio LDA.",
        "german": "%s ist ein kantenschärfender Kern, der NICHT für Imbio LDA empfohlen ist.",
        "spanish": "%s es un núcleo de realce con borde, NO se recomienda para LDA Imbio.",
        "italian": "%s è un kernel di miglioramento dei bordi, NON consigliato per Imbio LDA.",
        "french": "%s est un noyau à renforcement des bords, ce qui n'est PAS recommandé pour Imbio LDA.",
        "portuguese": "%s é um núcleo com aprimoramento delimitado, o que NÃO é recomendado para Imbio LDA.",
    },
    "edge_kernel_warning_select": {
        "english": "%s is an edge-enhancing kernel, which is NOT recommended for SeleCT.",
        "german": "%s ist ein kantenschärfender Kern, der NICHT für SeleCT empfohlen ist.",
        "spanish": "%s es un núcleo de realce con borde, NO se recomienda para SeleCT.",
        "italian": "%s è un kernel di miglioramento dei bordi, NON consigliato per SeleCT.",
        "french": "%s est un noyau à renforcement des bords, ce qui n'est PAS recommandé pour SeleCT.",
        "portuguese": "%s é um núcleo com aprimoramento delimitado, o que NÃO é recomendado para SeleCT.",
    },
    "unknown_kernel_warning": {
        "english": "%s is a kernel with unknown characteristics. Edge-enhancing kernels are NOT recommended for Imbio LDA.",
        "german": "%s ist ein Kern mit unbekannten Eigenschaften. Kantenschärfende Kerne werden für Imbio LDA nicht empfohlen.",
        "spanish": "%s es un realce con características desconocidas. Los núcleos de realce con borde NO se recomiendan para LDA Imbio.",
        "italian": "%s è un kernel con caratteristiche sconosciute. I kernel di miglioramento dei bordi NON sono consigliati per Imbio LDA.",
        "french": "%s est un noyau avec des caractéristiques inconnues. Les noyaux à renforcement des bords ne sont PAS recommandés pour Imbio LDA.",
        "portuguese": "%s é um núcleo com características desconhecidas. Núcleos com aprimoramento delimitado NÃO são recomendados para Imbio LDA.",
    },
    "slice_spacing_warning": {
        "english": "Slice spacing is inconsistent (%s%% of spacings are %s mm). If this is not expected, check the input DICOM series to ensure that no slices are missing.",
        "german": "Schichtabstand ist inkonsistent (%s%% des Abstands sind %s mm). Wird dies nicht erwartet, die DICOM-Eingangsserie überprüfen, um sicherzustellen, dass keine Schichten fehlen.",
        "spanish": "Espacios entre cortes inconsistentes (%s%% de los espacios son de %s mm). Si no es lo previsto, compruebe la serie de entrada de DICOM para asegurarse de que no faltan cortes.",
        "italian": "La distanza tra le sezioni è incoerente (il %s%% delle distanze corrisponde a %s mm). Se questo risultato è inatteso, controllare la serie DICOM in ingresso per garantire che non manchino sezioni.",
        "french": "L'espacement des coupes est incohérent (%s%% des espacements sont de %s mm). Si cela est inattendu, vérifiez la série DICOM saisie pour vous assurer qu'il ne manque pas de coupes.",
        "portuguese": "O espaçamento entre cortes é inconsistente (%s%% de espaçamentos são de %s mm). Se isso não for esperado, verifique a série DICOM de entrada para garantir que não faltam cortes.",
    },
    "no_lobe_warning": {
        "english": "No %s lobe identified in segmentation",
        "german": "Kein %s Lappen in Segmentierung festgestellt",
        "spanish": "No se identificó el lóbulo %s en la segmentación",
        "italian": "Nessun lobo %s identificato nella segmentazione",
        "french": "Aucun lobe %s identifié dans la segmentation",
        "portuguese": "Nenhum lobo %s identificado na segmentação",
    },
    "no_lung_warning": {
        "english": "No %s lung identified in segmentation",
        "german": "Keine %s Lunge in Segmentierung festgestellt",
        "spanish": "No se identificó el pulmón %s en la segmentación",
        "italian": "Nessun polmone %s identificato nella segmentazione",
        "french": "Aucun poumon %s identifié dans la segmentation",
        "portuguese": "Nenhum pulmão %s identificado na segmentação",
    },
}


class RoundingPrecision(Enum):
    LAA = 2
    PERC = 2
    VOL = 4
    ORIENTATION = 4  # Used for checking ImageOrientationPatient vector
    FISSURE_COMPLETENESS = 2
    SLICE_SPACING = 3
    SLICE_SPACING_LOW_RES = 3


class DicomOutputType(Enum):
    """Represents the output type we are creating.

    Note that PDF_REPORT is an encapsulated PDF report,
    while SC_REPORT is a secondary capture PDF report.
    """

    PDF_REPORT = 1
    SC_REPORT = 2
    RAW = 3
    RGB = 4

    @property
    def series_prefix_key(self):
        """Returns a `str` value to be used in the SeriesNumber"""
        return str(self.value)

    @property
    def output_type_name(self):
        """Returns a `str` name to be used in the SeriesDescription"""
        return _SERIES_DESCRIPTION_NAMES[self]


class SeriesType(Enum):
    """This class exists as a parent class to ReportType and SegmentationType.
    It contains common properties and class methods to avoid duplication
    of code, but contains no members of its own. As such, it should never be
    used directly.
    """

    @classmethod
    def has_value(cls, value):
        """Returns true if one of the members has `value` for its value.

        Allows checking for membership by string value.
        """
        all_values = {item.value for item in cls}
        return value in all_values

    @property
    def assessment_name(self):
        """Returns assessment name to be used in SeriesDescription"""
        return _SERIES_DESCRIPTION_NAMES[self]

    @property
    def derivation_description(self):
        """Returns `str` with deriviation description.

        May need to be formatted with values such as ithresh and ethresh
        before setting as DerivationDescription.
        """
        return _DERIVATION_DESCRIPTIONS[self]

    @property
    def series_prefix_key(self):
        """Returns a `str` value to be used in the SeriesNumber"""
        return _SERIES_TYPE_SERIES_PREFIXES[self]


class ReportType(SeriesType):
    PRM = "prm"
    SIA = "sia"
    PATIENT = "patient"
    QUITTER = "quitter"
    PRMHD = "prmhd"
    PATIENT_INFO = "patient_info"
    INPUT_CHECK = "input_check_failure"
    SELECT = "select"
    SELECT_QUALITY = "select_quality"

    @property
    def classification_labels(self):
        """Returns an enum containing the labels for this type"""
        return _REPORT_TYPE_CLASSIFICATIONS[self]

    @property
    def is_select(self):
        """Returns true if report type is select"""
        return self == self.SELECT

    @property
    def is_patient_report(self):
        """Returns true if report type is patient or quitter"""
        return self in (self.PATIENT, self.QUITTER)

    @property
    def product_name(self):
        """Returns the product name to be inserted on the second page
        header for this ReportType.
        """
        return _PRODUCT_NAMES[self]

    @property
    def report_uid_suffix(self):
        """Returns a `str` value to be used as a suffix for report uid.

        This way, the various reports generated for one run of the algorithm
        can have a shared base report uid while still being able to
        differentiate between them.
        """
        return _REPORT_UID_SUFFIXES[self]

    @property
    def latex_template_filename(self):
        """Returns the filename for the LaTex template for this ReportType"""
        return _LATEX_TEMPLATE_FILENAMES[self]


class SegmentationType(SeriesType):
    LUNGS = "lungs"
    LOBES = "lobes"
    MASK = "mask"
    FISSURES = "fissures"
    FISSURE_PROBABILITY = "fissure_probability"
    WARPED_MASK = "warped_mask"


class RegistrationType(SeriesType):
    INSP2EXP = "insp2exp"
    EXP2INSP = "exp2insp"


class SegmentationLabel(Enum):
    TOTAL = None
    RIGHT = 1
    LEFT = 2
    AIRWAYS = 3
    RIGHT_UPPER = 11
    RIGHT_MIDDLE = 13
    RIGHT_LOWER = 12
    LEFT_UPPER = 21
    LEFT_MIDDLE = 23
    LEFT_LOWER = 22
    AIRWAYS_LOBAR_SEXTANT = 30
    VESSELS = 50
    ##### Fissures
    LO_INCOMPLETE = 40
    LO_COMPLETE = 41
    RH_INCOMPLETE = 42
    RH_COMPLETE = 43
    RORML_INCOMPLETE = 44
    RORML_COMPLETE = 45
    RORUL_INCOMPLETE = 46
    RORUL_COMPLETE = 47
    RO_INCOMPLETE = 48
    RO_COMPLETE = 49
    #########
    LO = 4  # Left oblique
    RH = 5  # Right horizontal fissure
    RO = 6
    RORML = (
        7  # The part of the right oblique fissure on the right middle lobe lobe bounary
    )
    RORUL = (
        8  # The part of the right oblique fissure on the right upper lobe lobe bounary
    )

    @classmethod
    def label_to_array_index(cls, label):
        """Convert a sub-region label name to an integer used for FORTRAN array processing
        (The 'classifications' and 'histograms' variables in quantification.py)
        """
        converter = {
            "RIGHT_UPPER": 1,
            "RIGHT_MIDDLE": 2,
            "RIGHT_LOWER": 3,
            "LEFT_UPPER": 4,
            "LEFT_MIDDLE": 5,
            "LEFT_LOWER": 6,
        }
        return converter[label]

    @classmethod
    def all_lung_region_names(cls, lobes=False):
        """Get string names for TOTAL, RIGHT, LEFT, RIGHT_UPPER, ...
        Note that this does NOT include airways members.
        """
        return [label.name for label in cls.all_lung_regions(lobes)]

    @classmethod
    def subregion_names(cls, use_lobes):
        """Get string names for RIGHT_UPPER, RIGHT_MIDDLE, ..."""
        if use_lobes:
            return [label.name for label in cls.lobes()]
        else:
            return [label.name for label in cls.sextants()]

    @classmethod
    def lung_names(cls):
        """Get string names for RIGHT, LEFT"""
        return [label.name for label in cls.lungs()]

    @classmethod
    def lungs(cls, with_airways=False):
        """Get enum members RIGHT, LEFT"""
        lungs = [cls.RIGHT, cls.LEFT]
        if with_airways:
            lungs.append(cls.AIRWAYS)
        return lungs

    @classmethod
    def all_lung_regions(cls, lobes=False):
        """Get enum members for TOTAL, RIGHT, LEFT, RIGHT_UPPER, ...
        Note that this does NOT include airways members.
        """
        if lobes:
            return [cls.TOTAL] + cls.lungs() + cls.lobes()
        else:
            return [cls.TOTAL] + cls.lungs() + cls.sextants()

    @classmethod
    def sextants(cls, with_airways=False):
        """Get enum members RIGHT_UPPER, RIGHT_MIDDLE, ..."""
        sextants = [
            cls.RIGHT_UPPER,
            cls.RIGHT_MIDDLE,
            cls.RIGHT_LOWER,
            cls.LEFT_UPPER,
            cls.LEFT_MIDDLE,
            cls.LEFT_LOWER,
        ]
        if with_airways:
            sextants.append(cls.AIRWAYS_LOBAR_SEXTANT)
        return sextants

    @classmethod
    def lobes(cls, with_airways=False):
        """Get enum members RIGHT_UPPER, RIGHT_MIDDLE, ..."""
        lobes = [
            cls.RIGHT_UPPER,
            cls.RIGHT_MIDDLE,
            cls.RIGHT_LOWER,
            cls.LEFT_UPPER,
            cls.LEFT_LOWER,
        ]
        if with_airways:
            lobes.append(cls.AIRWAYS_LOBAR_SEXTANT)
        return lobes

    @classmethod
    def fissures(cls):
        """Get enum members RH_COMPLETE, RH_INCOMPLETE, ..."""
        fissures = [
            cls.RH_COMPLETE,
            cls.RH_INCOMPLETE,
            cls.RORML_COMPLETE,
            cls.RORML_INCOMPLETE,
            cls.RORUL_COMPLETE,
            cls.RORUL_INCOMPLETE,
            cls.LO_COMPLETE,
            cls.LO_INCOMPLETE,
        ]
        return fissures

    @classmethod
    def left_region_labels(cls):
        """Get label values for all left lung labels"""
        labels = [
            cls.LEFT,
            cls.LEFT_UPPER,
            cls.LEFT_MIDDLE,
            cls.LEFT_LOWER,
        ]
        return [label.value for label in labels]

    @classmethod
    def right_region_labels(cls):
        """Get label values for all left lung labels"""
        labels = [
            cls.RIGHT,
            cls.RIGHT_UPPER,
            cls.RIGHT_MIDDLE,
            cls.RIGHT_LOWER,
        ]
        return [label.value for label in labels]

    @classmethod
    def anatomical_fissure_names(cls):
        """Get enum members RH, LO, RO. These are the typical anatomical
        fissures and are NOT used for assessing surgical repair"""
        fissure_names = [cls.RH, cls.LO, cls.RO]
        return fissure_names

    @classmethod
    def lobar_fissure_names(cls):
        """Get enum members RH, LO, RORML, RORUL. These are the fissures surrounding
        the upper and lower left and right lobes and ARE used for assessing surgical repair"""
        fissure_names = [cls.RH, cls.LO, cls.RORML, cls.RORUL]
        return fissure_names

    @classmethod
    def all_fissure_names(cls):
        """Get enum members RH, LO, RORML, RORUL and RO. These are the fissures surrounding
        the upper and lower left and right lobes as well as the anatomical fissure names"""
        fissure_names = [cls.RH, cls.LO, cls.RORML, cls.RORUL, cls.RO]
        return fissure_names

    @classmethod
    def airways_labels(cls):
        """Return list of labels for airways"""
        airways = [cls.AIRWAYS, cls.AIRWAYS_LOBAR_SEXTANT]
        return [lbl.value for lbl in airways]


class SIAClassLabel(Enum):
    LAA = 1
    NORMAL = 2

    @classmethod
    def report_png_labels(cls):
        """Return list of labels for use in report png creation"""
        return [cls.LAA.value]


class LungMapClassLabel(Enum):
    """Matches values of SIAClassLabel, but labels used in report images differ"""

    LAA = SIAClassLabel.LAA.value
    NORMAL = SIAClassLabel.NORMAL.value

    @classmethod
    def report_png_labels(cls):
        """Return list of labels for use in report png creation"""
        return [lbl.value for lbl in cls]


class PRMClassLabel(Enum):
    NORMAL = 1
    FUNCTIONAL = 2
    PERSISTENT = 3
    UNCLASSIFIED = 4

    @classmethod
    def report_png_labels(cls):
        """Return list of labels for use in report png creation"""
        labels = [label for label in cls if label is not cls.UNCLASSIFIED]
        return [lbl.value for lbl in labels]


class PRMHDClassLabel(Enum):
    NORMAL = 1
    FUNCTIONAL = 2
    PERSISTENT = 3
    HYPERATTENUATION = 4
    UNCLASSIFIED = 5

    @classmethod
    def report_png_labels(cls):
        """Return list of labels for use in report png creation"""
        labels = [label for label in cls if label is not cls.UNCLASSIFIED]
        return [lbl.value for lbl in labels]


_REPORT_TYPE_CLASSIFICATIONS = {
    ReportType.SIA: SIAClassLabel,
    ReportType.PRM: PRMClassLabel,
    ReportType.PATIENT: LungMapClassLabel,
    ReportType.QUITTER: LungMapClassLabel,
    ReportType.PRMHD: PRMHDClassLabel,
}

_SERIES_TYPE_SERIES_PREFIXES = {
    ReportType.SIA: "00",
    ReportType.PRM: "01",
    ReportType.PATIENT: "02",
    ReportType.QUITTER: "03",
    ReportType.PRMHD: "04",
    ReportType.INPUT_CHECK: "09",
    ReportType.SELECT: "10",
    ReportType.SELECT_QUALITY: "11",
    SegmentationType.LUNGS: "90",
    SegmentationType.LOBES: "91",
    SegmentationType.MASK: "92",
    SegmentationType.FISSURES: "93",
    SegmentationType.WARPED_MASK: "94",
    SegmentationType.FISSURE_PROBABILITY: "95",
    RegistrationType.INSP2EXP: "81",
    RegistrationType.EXP2INSP: "82",
}
_SERIES_DESCRIPTION_NAMES = {
    ReportType.SIA: "LDA Insp",
    ReportType.PRM: "LDA Func",
    ReportType.PATIENT: "LDA LungMap",
    ReportType.QUITTER: "LDA LungMap NonSmoker",
    ReportType.PRMHD: "LDA Func HD",
    ReportType.INPUT_CHECK: "LDA Input Check",
    ReportType.SELECT: "SeleCT",
    ReportType.SELECT_QUALITY: "SeleCT Quality",
    SegmentationType.MASK: "LDA Lung Mask",
    SegmentationType.WARPED_MASK: "LDA Warped Lung Mask",
    SegmentationType.LUNGS: "LDA Lung Segmentation",
    SegmentationType.LOBES: "LDA Lung Lobe Segmentation",
    SegmentationType.FISSURES: "SeleCT Fissure Completeness",
    SegmentationType.FISSURE_PROBABILITY: "SeleCT Fissure Probability Map",
    DicomOutputType.PDF_REPORT: "Report",
    DicomOutputType.SC_REPORT: "Report",
    DicomOutputType.RAW: "Raw",
    DicomOutputType.RGB: "RGB",
    RegistrationType.INSP2EXP: "Lung Registration insp2exp",
    RegistrationType.EXP2INSP: "Lung Registration exp2insp",
}
# Note that sia, patient, and quitter outputs
# have the same derivation description.
_DERIVATION_DESCRIPTIONS = {
    ReportType.SIA: "Lung Density Analysis Map was created from the "
    "inspiration CT scan. Inspiration threshold used for "
    "classification is {ithresh} HU.{filter_status}",
    ReportType.PATIENT: "Lung Density Analysis Map was created from the "
    "inspiration CT scan. Inspiration threshold used for "
    "classification is {ithresh} HU.{filter_status}",
    ReportType.QUITTER: "Lung Density Analysis Map was created from the "
    "inspiration CT scan. Inspiration threshold used for "
    "classification is {ithresh} HU.{filter_status}",
    ReportType.PRM: "Lung Density Analysis Map was created from expiration "
    "and registered inspiration CT scans. Inspiration "
    "threshold used for classification is {ithresh} HU. "
    "Expiration threshold used for classification is "
    "{ethresh} HU.{filter_status}",
    ReportType.PRMHD: "Lung Density Analysis Map was created from expiration "
    "and registered inspiration CT scans. Inspiration "
    "threshold used for classification is {ithresh} HU. "
    "Expiration threshold used for classification is "
    "{ethresh} HU.{filter_status}",
    ReportType.INPUT_CHECK: "Imbio LDA Input Check Failure Report was "
    "created from the inspiration CT scan. Contents of the report detail why "
    "the input data was invalid for the analysis.",
    ReportType.SELECT: "Imbio LDA SeleCT Report was created from the "
    "inspiration CT scan. Inspiration threshold used for "
    "classification is {ithresh} HU.{filter_status}",
    ReportType.SELECT_QUALITY: "SeleCT Quality Report was created from the "
    "inspiration CT scan. Contents of the report detail why the input "
    "data was invalid for the analysis.",
    SegmentationType.LOBES: "Original CT scan was segmented to identify each "
    "lung lobe and the major airways for LDA Software.",
    SegmentationType.LUNGS: "Original CT scan was segmented to identify the "
    "left and right lungs and the major airways for LDA Software.",
    SegmentationType.MASK: "Original CT scan was segmented to identify the lungs and "
    "major airways for LDA Software.",
    SegmentationType.FISSURES: "Original CT scan was segmented to identify complete and incomplete "
    "pulmonary fissures.",
    SegmentationType.FISSURE_PROBABILITY: "Original CT scan was segmented to map probability "
    "of pulmonary fissures for fissure completeness calculations",
    SegmentationType.WARPED_MASK: "The moving image segmentation was warped to the fixed frame "
    "using registration displacement fields",
    RegistrationType.INSP2EXP: "Inspiratory CT registered to expiratory CT scan",
    RegistrationType.EXP2INSP: "Expiratory CT registered to inspiratory CT scan",
}
# These suffixes are used to differentiate between output reports.
# The values correspond to the product name (PRM is F for Functional, etc.)
_REPORT_UID_SUFFIXES = {
    ReportType.PRM: "F",
    ReportType.PRMHD: "H",
    ReportType.SIA: "I",
    ReportType.PATIENT: "P",
    ReportType.QUITTER: "Q",
    ReportType.INPUT_CHECK: "Q",
}
_LATEX_TEMPLATE_FILENAMES = {
    ReportType.SIA: "report_sia_{}.tex",
    ReportType.PRM: "report_prm_{}.tex",
    ReportType.PATIENT: "report_patient_{}.tex",
    ReportType.QUITTER: "report_quitter_{}.tex",
    ReportType.PRMHD: "report_prmhd_{}.tex",
    ReportType.PATIENT_INFO: "patient_information_{}.tex",
    ReportType.INPUT_CHECK: "input_check_report_{}.tex",
    ReportType.SELECT: "report_select_{}.tex",
    ReportType.SELECT_QUALITY: "input_check_report_select.tex",
}
_PRODUCT_NAMES = {
    ReportType.PRM: f"Imbio LDA Functional Assessment {ALG_VERSION}",
    ReportType.PRMHD: f"Imbio LDA Functional HD Assessment {ALG_VERSION}",
    ReportType.SIA: f"Imbio LDA Inspiration Assessment {ALG_VERSION}",
    ReportType.PATIENT: f"Imbio LDA LungMap Assessment {ALG_VERSION}",
    ReportType.QUITTER: f"Imbio LDA LungMap Non-Smoker Assessment {ALG_VERSION}",
}
