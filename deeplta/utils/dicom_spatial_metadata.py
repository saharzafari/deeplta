import numpy as np
from dicomhd import data_structures as dhd_ds
from dicomhd import metadata_utils as dhd_mu

# from imbio_uip import codes
from . import codes


def get_most_common_slice_spacing(dicom_series, rounding_precision=3):
    """This function gets the median slice spacing, which is the appropriate value to use for volume calculations
        when one or two slices are missing.

        dicom_series:  dicomhd.data_structures.DicomSeries containg the dicom data
        min_fraction: If not None, then this function will throw an
        error if the percentage of slice spacings that are equal to the most common slice spacing is less than min_median_fraction
        For example, if min_median_fraction = 0.9 and a dicom series with 500 slices, and error would be thrown if 51
        non-contiguous slices were missing

    #Returns:
        most common slice spacing 'float'
    """

    spacings = dhd_mu.get_slice_spacing_list(dicom_series, precision=rounding_precision)
    spacings = np.array(spacings)

    # Find most common spacing
    unique_spacings, counts = np.unique(spacings, return_counts=True)
    i = np.argmax(counts)
    most_common_spacing = unique_spacings[i]

    percentage = (spacings == most_common_spacing).mean()

    return most_common_spacing, percentage


def get_voxel_spacing(dicom_series, min_fraction=None):
    """Get the voxel spacing in all directions. Returns only positive values.
    #Arguments:
        dicom_series:  dicomhd.data_structures.DicomSeries containg the dicom data
    #Returns:
        tuple representing the voxel spacing. Returns absolute values in the case of negative spacing
    """

    z_spacing, percentage = get_most_common_slice_spacing(dicom_series)

    if min_fraction is not None:
        if percentage < min_fraction:
            if percentage < min_fraction:
                codes.raise_code(
                    "0004", message="The dicom series slice spacing is too inconsistent"
                )

    z_spacing = abs(z_spacing)

    dicoms = dhd_ds.extract_pydcm(dicom_series)
    pixel_spacings_x = list({float(s.PixelSpacing[0]) for s in dicoms})
    pixel_spacings_y = list({float(s.PixelSpacing[1]) for s in dicoms})

    if len(pixel_spacings_x) > 1:
        codes.raise_code(
            "0005",
            message="More than one value for the LR pixel spacing in the DICOM series",
        )

    if len(pixel_spacings_y) > 1:
        codes.raise_code(
            "0005",
            message="More than one value for the AP pixel spacing in the DICOM series",
        )

    x_spacing = np.absolute(pixel_spacings_x[0])
    y_spacing = np.absolute(pixel_spacings_y[0])
    z_spacing = np.absolute(z_spacing)

    return (z_spacing, y_spacing, x_spacing)


def get_origin(dicom_series):
    """Get the docm series slice stack origin
    #Arguments:
        dicom_series:  dicomhd.data_structures.DicomSeries containing the dicom data
    #Returns:
        tuple representing the physical location of the first index of the first slice
    """
    pydicoms = dhd_ds.extract_pydcm(dicom_series)
    positions = pydicoms[0].ImagePositionPatient
    positions = [float(p) for p in positions]
    return tuple(positions)
