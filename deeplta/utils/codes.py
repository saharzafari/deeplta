# ==============================================================================
# Copyright (c) Imbio. All rights reserved.
#
# codes.py - Defines and prints errors and warnings for Imbio Lung PRM
# ==============================================================================
import collections

from .logger import AlgorithmFailedException
from .logger import lda_logger

## fieldnames info
# level
#   level of code to be raised, can be the following options
#           debug
#           info
#           warning
#           error
#
# message
#   description of error
#
# details
#   more verbose description of error and possible reason for error
#
# returncode

#   None: return code N/A because level does not equal 'error'

fieldnames = "category,level,returncode"
ErrorEntry = collections.namedtuple("ErrorEntry", fieldnames)

codes = {}

"""
This is essentially an unhandled error. This usually happens
when the application runs out of memory or otherwise is terminated by the operating
system. It is possible that it is a bug. This type of error is normally thrown when
the return code from one of the imbio cli commands is 1
"""
level = "error"
category = "Unexpected Error"
details = (
    "DETAILS: This is essentially an unhandled error. This usually happens "
    + "when the application runs out of memory or otherwise is terminated by the operating "
    + "system. It is possible that it is a bug. This type of error is normally thrown when "
    + " the return code from one of the imbio cli commands is 1"
)
returncode = 99
codes["0099"] = ErrorEntry(category=category, level=level, returncode=returncode)

"""
These are errors for things like 'output dir already exists' or 'input dir doesnt exist'
"""
level = "error"
category = "IO Error"
returncode = 2
codes["0002"] = ErrorEntry(category=category, level=level, returncode=returncode)

"""
This error code is only for errors in the category of "Contains more than 1 series".
This is the error code ICCP looks for when determining whether to up the job in an
"INPUT REVIEW" status.
"""
level = "error"
category = "Invalid input data"
returncode = 3
codes["0003"] = ErrorEntry(category=category, level=level, returncode=returncode)

"""
These are errors for things like 'slice thickness too large' or 'FOV too large'
"""
level = "error"
category = "Unacceptable input data"
returncode = 4
codes["0004"] = ErrorEntry(category=category, level=level, returncode=returncode)

"""
These are errors that should not occur (i.e. an unexpected condition occurs in the code)
"""
level = "error"
category = "Unexpected condition"
returncode = 5
codes["0005"] = ErrorEntry(category=category, level=level, returncode=returncode)


"""
These are errors that are related to external executables failing
"""
level = "error"
category = "Executable failed"
returncode = 6
codes["0006"] = ErrorEntry(category=category, level=level, returncode=returncode)


"""
These are errors that are related to command line input formats that are not valid
"""
level = "error"
category = "Improperly formatted command line argument"
returncode = 7
codes["0007"] = ErrorEntry(category=category, level=level, returncode=returncode)


"""
These are errors that are related to improperly installed software
"""
level = "error"
category = "Invalid software package"
returncode = 8
codes["0008"] = ErrorEntry(category=category, level=level, returncode=returncode)


"""
These are errors that are related to segmentation
"""
level = "error"
category = "Segmentation error"
returncode = 9
codes["0009"] = ErrorEntry(category=category, level=level, returncode=returncode)


"""
These are errors that are related to registration
"""
level = "error"
category = "Registration error"
returncode = 10
codes["0010"] = ErrorEntry(category=category, level=level, returncode=returncode)

"""
These are errors that are related to incorrect usage of code
"""
level = "error"
category = "Incorrect Usage"
returncode = 11
codes["0011"] = ErrorEntry(category=category, level=level, returncode=returncode)


######################## Warnings ##################

# Leaving this section here until PRM is implemented for message reference

# """Not for clinical use warning"""
# level = "warning"
# category = "Not for Clinical Use"
# returncode = None
# codes["1100"] = ErrorEntry(category=category, level=level, returncode=returncode)


# level = "warning"
# category = "User has given a segmentation mask for analysis. Imbio lung segmentation will not be performed."
# returncode = None
# codes["1101"] = ErrorEntry(category=category, level=level, returncode=returncode)


# level = "warning"
# category = "One lung detected"
# returncode = None
# codes["1102"] = ErrorEntry(category=category, level=level, returncode=returncode)

# level = "warning"
# category = "No gender provided to the non-smoker LungMap report"
# returncode = None
# codes["1103"] = ErrorEntry(category=category, level=level, returncode=returncode)


# level = "warning"
# category = "Could not split into sextants"
# returncode = None
# codes["1104"] = ErrorEntry(category=category, level=level, returncode=returncode)


# level = "warning"
# category = "Lobe is missing"
# returncode = None
# codes["1105"] = ErrorEntry(category=category, level=level, returncode=returncode)


def raise_code(code, message="", failure_report_details=None):
    """Raises warning or error based on given code number.
    Also writes warning or error to the json_filename

    # Arguments
        code (str): unique code number associated with warning/error

        message (str): additional information to be logged.

        failure_report_details: (optional) if given, passes results
        on to exception, so that an output pdf with the failure report
        can be generated.
    """
    # Get "level" of exception -- warning or error or info etc
    level = codes[code].level
    category = codes[code].category

    # Ensure that only multiple series errors are passed to error code 0003
    if code == "0003" and "More than one series" not in message:
        code = "0002"

    # Create code string with error code and error message
    code_string = f"{code} | {category} | {message}"
    return_code = codes[code].returncode

    # Print WARNING
    if level == "warning":
        lda_logger.terminal_warning(code_string)

    # Print ERROR
    if level == "error":
        lda_logger._return_code = return_code
        raise AlgorithmFailedException(
            code_string, code, return_code, failure_report_details
        )
