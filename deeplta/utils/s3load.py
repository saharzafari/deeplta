
import sys
import os
import threading
import time
from datetime import timedelta
import pathlib
import ntpath
import argparse
import json
import logging
import boto3
from checksum import verify_md5sum


UNIT_COEFF = dict(
    KB=1024,
    MB=1024**2,
    GB=1024**1,
)


DEFAULT_DOWNLOAD_CONFIG = dict(
    multipart_threshold=25 * UNIT_COEFF['MB'],
    max_concurrency=10,
    multipart_chunksize=25 * UNIT_COEFF['MB'],
    use_threads=True
)

DEFAULT_UPLOAD_CONFIG = dict(
    multipart_threshold=25*UNIT_COEFF['MB'],
    max_concurrency=10,
    multipart_chunksize=25*UNIT_COEFF['MB'],
    use_threads=True
)


class ProgressPercentage(object):
    def __init__(self, filename, size=None, prefix_str=''):
        self._filename = filename
        if size is None:
            self._size = float(os.path.getsize(filename))
        else:
            self._size = size
        self._prefix_str = prefix_str
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r%s %s / %s  (%.2f%%)" % (
                    self._prefix_str,
                    self._seen_so_far,
                    self._size,
                    percentage))
            sys.stdout.flush()


def fupload_s3(file_path, bucket_name, object_name=None, **kwargs):
    start = time.time()
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = ntpath.basename(file_path)
    # Upload the file
    boto3.setup_default_session(profile_name='sahar-dev-ec2')
    s3_client = boto3.client("s3")

    checksum_verification = kwargs.get('checksum_verification', True)
    multipart_chunksize = kwargs['config'].get('multipart_chunksize', None)
    try:
        config = boto3.s3.transfer.TransferConfig(**kwargs['config'])
        # Perform the transfer
        s3_client.upload_file(
            file_path,
            bucket_name,
            object_name,
            Config=config,
            Callback=ProgressPercentage(
                file_path,
                prefix_str='Uploading file {:} -> {:}.{:}: '.format(
                    os.path.dirname(file_path), bucket_name, object_name,)),
        )
        sys.stdout.write('\nUploading "{:}" to "{:}.{:}" completed.'.format(file_path, bucket_name, object_name))
        elapsed = time.time() - start
        sys.stdout.write(' [ElapsedTime:{:}]\n'.format(str(timedelta(seconds=elapsed))))
        if checksum_verification:
            if multipart_chunksize is None:
                raise ValueError('"chunk_size" is required for checksum verification.')
            sys.stdout.write('Checksum verification started...\n')
            if not verify_md5sum(bucket_name, object_name, file_path, multipart_chunksize):
                raise Warning('Checksum verification failed! The uploaded file may be inaccurate.')
            else:
                sys.stdout.write('Checksum verification succeeded! The uploaded file is accurate.\n')
    except Exception as e:
        raise RuntimeError("Error in uploading: {}".format(e))


def fdownload_s3(file_path, bucket_name, object_name=None, **kwargs):
    start = time.time()
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = ntpath.basename(file_path)

    session = boto3.Session()
    s3_client = session.client("s3")
    response = s3_client.head_object(Bucket=bucket_name, Key=object_name)
    size = response['ContentLength']
    checksum_verification = kwargs.get('checksum_verification', True)
    multipart_chunksize = kwargs['config'].get('multipart_chunksize', None)
    # Download the file
    try:
        config = boto3.s3.transfer.TransferConfig(**kwargs['config'])
        # Perform the transfer
        response = s3_client.download_file(
            bucket_name,
            object_name,
            file_path,
            Config=config,
            Callback=ProgressPercentage(
                file_path,
                size=size,
                prefix_str='Downloading file {:}.{:} -> {:}:'.format(
                    bucket_name, object_name, os.path.dirname(file_path)))
        )
        sys.stdout.write('\nDownloading from {:}.{:} to {:} completed.'.format(bucket_name, object_name, file_path))
        elapsed = time.time() - start
        sys.stdout.write(' [ElapsedTime:{:}]\n'.format(str(timedelta(seconds=elapsed))))
        if checksum_verification:
            if multipart_chunksize is None:
                raise ValueError('"chunk_size" is required for checksum verification.')
            sys.stdout.write('Checksum verification started...\n')
            if not verify_md5sum(bucket_name, object_name, file_path, multipart_chunksize):
                raise Warning('Checksum verification failed! The downloaded source file may be inaccurate.')
            else:
                sys.stdout.write('Checksum verification succeeded! The downloaded file is accurate.\n')
    except Exception as e:
        raise RuntimeError("Error in downloading: {}".format(e))


def _run(**kwargs):
    if 'config' in kwargs:
        if 'multipart_threshold' in kwargs['config']:
            unit = kwargs['config']['multipart_threshold'][-2:]
            kwargs['config']['multipart_threshold'] = \
                int(kwargs['config']['multipart_threshold'][:-2]) * UNIT_COEFF[unit]
        if 'multipart_chunksize' in kwargs['config']:
            unit = kwargs['config']['multipart_chunksize'][-2:]
            kwargs['config']['multipart_chunksize'] = \
                int(kwargs['config']['multipart_chunksize'][:-2]) * UNIT_COEFF[unit]
        if 'io_chunksize' in kwargs['config']:
            unit = kwargs['config']['io_chunksize'][-2:]
            kwargs['config']['io_chunksize'] = \
                int(kwargs['config']['io_chunksize'][:-2]) * UNIT_COEFF[unit]

    if kwargs['mode'] == 'download':
        config = kwargs.get('config', DEFAULT_DOWNLOAD_CONFIG)
        fdownload_s3(
            kwargs['path'],
            bucket_name=kwargs['bucket_name'],
            object_name=kwargs['object_name'],
            config=config,
        )
    elif kwargs['mode'] == 'upload':
        config = kwargs.get('config', DEFAULT_UPLOAD_CONFIG)
        fupload_s3(
            kwargs['path'],
            bucket_name=kwargs['bucket_name'],
            object_name=kwargs['object_name'],
            config=config,
        )
    else:
        raise ValueError('"mode" can either be "download" or "upload".')


def run(**params):

    _run(**params)


def main():
    conf_default_path = '{:}/conf/params_s3load.json'.format(os.path.join(pathlib.Path().absolute()))
    parser = argparse.ArgumentParser(description='Transform files from/to an AWS-S3 object.')
    parser.add_argument("-v", "--verbose",
                        help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-p", "--params-path",
                        help="path to module json parameters file",
                        default=conf_default_path,
                        type=str, )

    args = parser.parse_args()
    if not os.path.exists(args.params_path):
        raise IOError('Params JSON file "{:}" does not exist!'.format(args.params_path))

    with open(args.params_path) as f:
        params = json.load(f)

    run(**params)


if __name__ == '__main__':
    main()
