"""
Copyright (c) Imbio. All rights reserved.

io.py - UIP Algorithm I/O functions
"""
import errno
import json
import os
import pathlib
import shutil
import time
import typing

import nibabel
import numpy as np
import yaml
from dicomhd import data_structures as dhd_ds
from dicomhd import generate_output_series as dhd_gos
from dicomhd import input_checks as dhd_ic
from dicomhd import io as dhd_io

from . import codes
# from imbio_uip import dicom_tags
# from imbio_uip import reporting
# from .definitions import NIFTI_CONVERSION_MATRIX
# from imbio_uip.definitions import TEMPDIR
# from imbio_uip.logger import logger


def write_dicom_dir(dcm_series, outdir):
    """Writes dicom series to outdir"""
#     if os.path.exists(outdir):
        # Raise error if report directory already exists
#         codes.raise_code("0002", message="Output directory %s already exists" % outdir)
    dhd_io.write_series(dcm_series, outdir)


def create_dicom_sr(original_series, series_type, output_type, uip_result, research):
    """Create DICOM Structured Report.

    # Arguments
        original_series (`dicomhd.DicomSeries`): Original inspiration series
        used as source of DICOM metadata values.

        series_type (`ReportType` or `SegmentationType`): type of series or
        segmentation that this metadata is for.

        output_type (`DicomOutputType`): Type of DICOM output being produced.

        research (`bool`): Flag indicating research usage. If True,
        'For Investigational Use Only' phrase placed in PDF report and output
        series descriptions.
    """
    logger.debug("create_dicom_sr")

    output_dcm_sr = dhd_gos.create_structured_report_dicom(original_series)

    output_dcm_sr = dicom_tags.populate_uip_dicom_metadata(
        output_dcm_sr,
        original_series,
        series_type,
        output_type,
        uip_result,
        research,
    )

    return output_dcm_sr


def read_series_uip(dirname, pixeldata=True, sort_dimensions=("thru_plane_position",)):
    """Reads in 2D image slices from given directory and
    places 2D slices into a `DicomSeries` object.
    Fails if multiple series or no series are found in
    given directory.

    NOTE: EncapsulatedPDF series do not have pixel data,
    so in order to read an ePDF algorithm output, the
    pixeldata=False argument should be used.

    # Arguments
        dirname (`str`): Path to directory containing series
            to be read in
        pixeldata (`bool`): if False, image pixel data should
            NOT be stored in DicomSeries object
        sort_dimensions: the values to sort the series by. This
            should be a tuple (don't forget the trailing comma if
            it has a length of one). Ex value: ("ImagePositionPatient",)

    # Returns
        A `DicomSeries` object
    """

    logger.debug("read_series_uip")

    if not os.path.isdir(dirname):
        raise NotADirectoryError(dirname)

    patients = dhd_io.sort_dcm_directory(dirname, sort_dimensions=sort_dimensions)
    series = tuple(dhd_ds.series_generator(patients))
    if len(series) > 1:
        codes.raise_code(
            "0003", message="Input directory %s contains more than one series" % dirname
        )
    try:
        dcm_series = dhd_io.read_series(dirname)
    except Exception:
        codes.raise_code("0002", message="Input directory %s does not exist" % dirname)
        dcm_series = []
    if pixeldata:
        dcm_series = dhd_io.load_series(dcm_series)

    return dcm_series


def checkdirs(indir, outdir):
    """Checks input and output directories. Checks directories exist
        and that the output directory is empty.

    # Arguments
        indir (str): Path to input directory

        outdir (str): path of directory to write output

    # Return

    # Raises
        AlgorithmFailedException if checks do not pass

    """
    logger.debug("checkdirs")

    # check input directory exists
    if not os.path.exists(indir):
        codes.raise_code("0002", message="Input directory %s does not exist" % indir)

    # if outdir does not exist, mkdir
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    # check outdir is empty
    outdirls = os.listdir(outdir)
    if outdirls:
        codes.raise_code("0002", message="Output directory %s is not empty" % outdir)

    return


def cleanup(tempdir):
    """Removes temporary working directory

    # Arguments
        tempdir (str): path to temporary directory

    # Return

    # Raises
        Debug message that tmp working directory could not be removed

    """
    # Try to delete the folder 3 times.  This is in case the file being deleted is locked for some reason
    # If we try again, we might be able to delete it a second or third time

    if not os.path.isdir(tempdir):
        logger.debug('cleanup: "%s" does not exist' % tempdir)

    for _ in range(3):
        try:
            shutil.rmtree(tempdir)
            return  # Break out of the function if an exception was not thrown
        except OSError:
            time.sleep(1)

    logger.debug("cleanup: Could not remove temporary working directory %s" % tempdir)


def cleanup_except(
    tempdir: typing.Union[str, pathlib.Path],
    except_: typing.Collection[typing.Union[str, pathlib.Path]],
) -> None:
    """Deletes a directory except for a collection of sub-directories and files.

    # Arguments
    tempdir (str): Root directory to delete all sub-directories and files

    # Return

    # Raises
        OSError

    """
    tempdir = pathlib.Path(tempdir).resolve()
    except_ = {(tempdir / pathlib.Path(x)).resolve() for x in except_}

    def is_subdir(potential_subdir: pathlib.Path, parent_dir: pathlib.Path) -> bool:
        return (potential_subdir == parent_dir) or (
            parent_dir in potential_subdir.parents
        )

    files_to_delete: typing.List[pathlib.Path] = []
    dirs_to_delete: typing.List[pathlib.Path] = []
    for root, dirs, files in os.walk(tempdir):
        root = pathlib.Path(root).resolve()
        files = [root / x for x in files]
        # We've found a complete exception directory so stop here.
        if root in except_:
            dirs.clear()
        else:
            # Delete all files not explicitly called out in except_.
            for file in files:
                if file not in except_:
                    files_to_delete.append(file)

            # If any of the exception directories
            # is a sub-directory of the current directory
            # then we don't want to delete the current
            # directory but we still want to recurse.
            for except_dir in except_:
                if is_subdir(except_dir, root):
                    break
            else:
                dirs_to_delete.append(root)
                dirs.clear()

    for dir in dirs_to_delete:
        cleanup(str(dir))
    for file in files_to_delete:
        try:
            os.remove(file)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise


def create_tempdir(outdir):
    """Creates temporary directory, 'tmp', as a working directory
    to save intermediate files during registration and classification.

    # Arguments
        outdir (str): path of directory where tmp directory is created

    # Returns
        tempdir (str): pathname of temporary directory
    """
    tempdir = os.path.join(outdir, TEMPDIR)
    if os.path.exists(tempdir):
        codes.raise_code("0002", message="tmp directory %s already exists" % tempdir)
    os.mkdir(tempdir)
    return tempdir


def read_json(json_filename):
    """Reads in data from json_filename in JSON

    # Arguments
        json_filename (str): path to the JSON file to read
    """
    with open(json_filename) as fp:
        udi_contents = fp.read()
    return json.loads(udi_contents)


def write_json(content_dict, json_filename):
    """Writes content_dict to json_filename in JSON

    Creates json_filename if it does not already exist.
    If the file exists, the content_dict is appended to
    the end of the file.

    # Arguments
        content_dict (dict): dictionary containing information

        json_filename (str): filename to write content_dict to
    """
    # If file exists, read in contents already in file
    if os.path.exists(json_filename):
        with open(json_filename) as fip:
            contents = fip.read()
        contents_list = json.loads(contents)
        contents_list[0].update(content_dict)
    # Otherwise create list with content_dict to write to
    else:
        contents_list = [content_dict]

    # Write out all contents in list
    with open(json_filename, "w") as fip:
        json.dump(contents_list, fip, indent=4, sort_keys=True)


def perform_input_checks(
    dcm_series,
    reports_outdir,
    languages,
    input_check_config=None,
    no_input_checks=False,
    research_label=None,
    logo_pathname=None,
):
    """Performs UIP input checks based on fields present in an input yaml
    config file. If any fields are missing, the associated input check will not be
    performed. If the config file is not provided or cannot be read, this function
    will pass in a yml object populated with default values.

    Throws error/warning if errors found

    # Arguments
        dcm_series (dicomhd.DicomSeries): original DICOM information

        reports_outdir (str): filename to write reports to

        languages ([str]): languages for output reports

        input_check_config (str): path to input check config file

        research_mode (bool): Flag indicating research usage. If True, warnings are output instead
            of errors if input checks fail

        research_label (bool): Flag indicating research usage. If True, 'For Investigational Use Only'
            phrase placed in PDF report and output series descriptions.

        logo_pathname (str): Pathname to logo directory
    """

    logger.debug("input_check")

    try:
        # Read YAML file
        with open(os.path.join(input_check_config), "r") as stream:
            cfg_yml = yaml.safe_load(stream)
    except Exception:
        # default values if config file not found
        cfg_yml = {
            "series_uid": "valid",
            "modality": "CT",
            "min_rev_time": 200,
            "max_rev_time": 1000,
            "max_col_spacing": 2.0,
            "max_row_spacing": 2.0,
            "min_fov": "(100, 100, 200)",
            "image_orientations": [
                [1, 0, 0, 0, 1, 0],
                [-1, 0, 0, 0, -1, 0],
                [-1, 0, 0, 0, 1, 0],
                [1, 0, 0, 0, -1, 0],
            ],
            "rounding_precision": 4,
            "max_slice_spacing": 2.0,
            "max_slice_thickness": 2,
            "rescale_type": "HU",
            "min_age": 18,
            "kernel": "valid",
        }

    known_kernels = _get_known_acceptable_kernels()

    if dcm_series is not None:
        (
            input_check_results,
            logger_warning,
            ss_warning,
        ) = dhd_ic.perform_all_input_checks(dcm_series, cfg_yml, known_kernels)

    if logger_warning:
        logger.warning(logger_warning)

    if any(not ic.is_pass for ic in input_check_results):
        if not no_input_checks:
            # generate failure report
            input_check_paths = []
            for language in languages:
                if language == "english":
                    input_check_paths.append(
                        reporting.input_check_report(
                            reports_outdir,
                            dcm_series,
                            language,
                            input_check_results,
                            research_label,
                            logo_pathname,
                        )
                    )
                else:
                    input_check_results_translated = dhd_ic.translate_input_checks(
                        input_check_results, language
                    )
                    input_check_paths.append(
                        reporting.input_check_report(
                            reports_outdir,
                            dcm_series,
                            language,
                            input_check_results_translated,
                            research_label,
                            logo_pathname,
                        )
                    )

            cleanup_except(reports_outdir, input_check_paths)
            # throw error
            codes.raise_code(
                "0004",
                message=sum(
                    [
                        [
                            ic.label,
                            f"expected: {ic.requirement}, actual: {ic.value}, units: {ic.units}, status: {ic.status.value}",
                        ]
                        for ic in input_check_results
                        if not ic.is_pass
                    ],
                    [],
                ),
            )
        else:
            # if input failures, only print to terminal
            logger.warning(
                sum(
                    [
                        [
                            ic.label,
                            f"expected: {ic.requirement}, actual: {ic.value}, units: {ic.units}, status: {ic.status.value}",
                        ]
                        for ic in input_check_results
                        if not ic.is_pass
                    ],
                    [],
                ),
            )
    # return results for testing
    return input_check_results


def _get_known_acceptable_kernels():
    """Returns a list of convolution kernels that are known to be acceptable for UIP

    Note: returns list all in uppercase, so should be compared with uppercase string
    """
    acceptable_kernels = [
        # GE - 1st Preference
        "STANDARD",
        "BONE",
        # GE - 2nd Preference
        "SOFT",
        # Philips - 1st Preference
        "B",
        "C",
        # Philips - 2nd Preference
        "L",
        # Siemens - 1st Preference
        "B31f",
        "B31s",
        "B35f",
        "B35s",
        "B45f",
        "B46f",
        # Siemens - 2nd Preference
        "B20",
        "B40",
        # Toshiba 1st Preference
        "Fc01",
        "Fc13",
        "Fc14",
        "Fc19",
        # Toshiba 2nd Preference
        "Fc05",
        "Fc18",
    ]

    acceptable_kernels = [kernel.upper() for kernel in acceptable_kernels]

    return acceptable_kernels


def np_export_nifti(
    output_nifti_file, np_array, spacing=None, origin=None, intent=1001
):
    """Export a numpy array as nifti.
    IMPORTANT: This function assumes the the pixel data is stored such that the
    voxel indices in the first dimenison increase from inferior to superior,
    the voxel indices in the second dimension increase from anterior to psoterior,
    and that the voxels in the third dimension increase from right to left

    Arguments
        output_nifti_file: output file name
        np_array: numpy.ndarray. Pixel data array in z,y,x ordering
        spacing: tuple. voxel spacing
        origin: tuple. first voxel physical coordinate
        intent: int. NIFTI intent code. Most medical images should have this as 1001. Displacement vectors can be 1006 or 1007
    :type output_nifti_file: : str

    :param np_array: input numpy array. Should be 3d
    :type np_array: : numpy.ndimage
    """

    if spacing is None:
        spacing = (1.0, 1.0, 1.0)

    if origin is None:
        origin = (0.0, 0.0, 0.0)

    # dicomhd is z,y,x
    # nifti is x,y,z
    spacing = tuple(np.flip(np.array(spacing)).tolist())
    origin = tuple(np.flip(np.array(origin)).tolist())

    T = np.eye(4)
    for k in range(3):
        T[k, k] = spacing[k]
        T[k, 3] = origin[k]

    # Convert from LPS to RAS
    
    NIFTI_CONVERSION_MATRIX = np.eye(4).astype(float)
    NIFTI_CONVERSION_MATRIX[0, 0] = -1.0
    NIFTI_CONVERSION_MATRIX[1, 1] = -1.0
    T = np.matmul(NIFTI_CONVERSION_MATRIX, T)

    # Create nifti image
    # dicomhd is z,y,x
    # nifti is x,y,z
    hdr = nibabel.Nifti1Header()
    hdr.set_data_dtype(np_array.dtype)
    hdr.set_intent(intent)
    hdr.set_qform(T, code=1)
    nifti = nibabel.Nifti1Image(np.swapaxes(np_array, 0, 2), T, hdr)
    nifti.to_filename(output_nifti_file)
