# ==============================================================================
# Copyright (c) Imbio. All rights reserved.
#
# logger.py - logging class for algorithms
# ==============================================================================
import inspect
import logging
import sys

from .definitions import LANGUAGE_UTILS
from .definitions import WARNINGS


class AlgorithmFailedException(Exception):
    def __init__(self, message, errorcode, returncode, failure_report_details=None):
        self.message = message
        self.errorcode = errorcode
        self.returncode = returncode
        self.failure_report_details = failure_report_details

    def __str__(self):
        return self.message


class ImbioLogger:
    def __init__(self):
        self._logger = logging.getLogger(__name__)
        stderr = logging.StreamHandler(sys.stderr)
        stderr.setLevel(logging.WARNING)
        stdout = logging.StreamHandler(sys.stdout)
        stdout.setLevel(logging.DEBUG)
        # Stdout shouldn't output WARNING or above
        stdout.addFilter(lambda record: record.levelno <= logging.INFO)
        # Both handlers have the same formatter
        FORMAT = "%(asctime)s | %(message)s"
        DATEFORMAT = "%Y-%m-%d %H:%M:%S"
        self._formatter = logging.Formatter(fmt=FORMAT, datefmt=DATEFORMAT)
        for handler in (stdout, stderr):
            handler.setFormatter(self._formatter)
            self._logger.addHandler(handler)
        self._logger.setLevel(logging.DEBUG)

        # initialize variable for writing out report file
        self._lines = []
        # initialize dict of warnings for inclusion on report
        self._report_warnings = {"english": []}

        self._return_code = -1
        self._error_code = ""

    def add_languages(self, languages):
        for language in languages:
            self._report_warnings[language] = []

    def addTXTFile(self, txt_file_name):
        filehandler = logging.FileHandler(txt_file_name)
        filehandler.setFormatter(self._formatter)
        filehandler.setLevel(logging.DEBUG)
        self._logger.addHandler(filehandler)

    def info(self, msg):
        frame_records = inspect.stack()[1]
        calling_module = inspect.getmodulename(frame_records[1])
        newmsg = f"INFO | {calling_module} | {msg}"
        self._lines.append(newmsg)
        self._logger.info(newmsg)

    def debug(self, msg):
        frame_records = inspect.stack()[1]
        calling_module = inspect.getmodulename(frame_records[1])
        newmsg = f"DEBUG | {calling_module} | {msg}"
        self._lines.append(newmsg)
        self._logger.debug(newmsg)

    def terminal_warning(self, msg):
        """This function takes in a warning message and writes it directly to the terminal"""
        frame_records = inspect.stack()[1]
        calling_module = inspect.getmodulename(frame_records[1])
        new_msg = f"WARNING | {calling_module} | {msg}"
        self._lines.append(new_msg)
        self._logger.warning(new_msg)

    def report_warning_lookup(self, msg, values=None, add_to_report=False):
        """This function takes in a key and looks up a message in a dict, formats it, and
        adds it to a warning dict for each language a report will be generated in if required.

        Params:
        msg = key to look up warning in dict
        values = variables to insert into message if appropriate
        add_to_report = whether the warning will be added to the output report
        """
        if not values:
            values = []
        if add_to_report:
            for key in self._report_warnings:
                variables = values
                if msg in ["no_lung_warning", "no_lobe_warning"]:
                    variables = [LANGUAGE_UTILS[values[0].title()][key].upper()]
                warning = WARNINGS[msg][key] % tuple(variables)
                self._report_warnings[key].append(warning)
                if key == "english":
                    terminal_warning = warning
        else:
            terminal_warning = WARNINGS[msg]["english"] % tuple(values)
        self.terminal_warning(terminal_warning)

    def error(self, msg):
        frame_records = inspect.stack()[1]
        calling_module = inspect.getmodulename(frame_records[1])
        newmsg = f"ERROR | {calling_module} | {msg}"
        self._lines.append(newmsg)
        self._logger.error(newmsg)

    def critical(self, msg):
        frame_records = inspect.stack()[1]
        calling_module = inspect.getmodulename(frame_records[1])
        newmsg = f"CRITICAL | {calling_module} | {msg}"
        self._lines.append(newmsg)
        self._logger.critical(newmsg)

    def setLevel(self, lvl):
        self._logger.setLevel(lvl)

    def getLines(self):
        return self._lines.copy()

    def get_report_warnings(self, language="english"):
        return self._report_warnings[language].copy()

    def _reset_report_warnings(self):
        self._report_warnings = {"english": []}


lda_logger = ImbioLogger()
