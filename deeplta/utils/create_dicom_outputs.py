"""
Copyright (c) Imbio. All rights reserved.

create_dicom_outputs.py
"""
from subprocess import CalledProcessError

from dicomhd import generate_output_series as dhd_gos

from . import codes
from . import dicom_tags
from .definitions import DicomOutputType
from .logger import lda_logger


def create_dicom(
    original,
    pixel_data,
    research_label,
    series_type,
    set_info=None,
    ithresh=None,
    ethresh=None,
    filter_on=None,
    is_select=False,
    fc_overread=None,
):

    """Creates a DicomSeries object with the required pixel data and metadata.

    # Arguments
        original (`DicomSeries`): original inspiration or expiration image used
        as background for rgb overlay and source of DICOM metadata values.

        pixel_data (`numpy.ndarray`): Pixel data for DICOM series. If output
        type is an RGB overlay, has 3 channels; otherwise, has 1 channel.

        research_label (`bool`): Flag indicating research labelling to be
        placed in outputs. If True, 'For Investigational Use Only' phrase placed in
        output series descriptions.

        series_type (`ReportType` or `SegmentationType`): type of series or
        segmentation that this metadata is for.

        set_info (`set_integration.SETinfo`): Contains information on
        such as SET version to be used in populating DICOM metadata
        if SET editor was used on input segmentation.

        ithresh (`int`): inspiration threshold.

        ethresh (`int`): expiration threshold. Required for PRM outputs.

        filter_on (`bool`): True if median filtering was used

        is_select (`bool`): True if SeriesDescription should
        begin with "SeleCT" instead of "LDA"

    # Returns
        A `DicomSeries` object with the metadata and pixel data populated.
    """
    is_rgb = len(pixel_data.shape) == 4 and pixel_data.shape[-1] == 3
    output_type = DicomOutputType.RGB if is_rgb else DicomOutputType.RAW

    out_dcm_series = dhd_gos.create_secondary_capture_series(
        original, pixel_data, is_rgb
    )

    out_dcm_series = dicom_tags.populate_lda_dicom_metadata(
        out_dcm_series,
        original,
        series_type,
        output_type,
        research_label,
        set_info=set_info,
        ithresh=ithresh,
        ethresh=ethresh,
        filter_on=filter_on,
        is_select=is_select,
        fc_overread=fc_overread,
    )

    # Before returning dicom image, assert patient attributes match original
    dicom_tags.assert_patient_attributes_match(
        original, out_dcm_series, ("PatientName", "PatientID", "StudyInstanceUID")
    )

    return out_dcm_series


def create_pdf_dicom(
    pdfname,
    ct_series,
    report_type,
    research_label,
    ithresh,
    ethresh=None,
    set_info=None,
    filter_on=None,
    language_tag=None,
    fc_overread=None,
):
    """Creates SOP Class for pdf report

    # Arguments

        pdfname: path to directory of saved pdf report

        ct_series (`DicomSeries`): input inspiration image

        report_type (`ReportType`): indicates type of report generated.

        research_label (`bool`): Flag indicating research labelling to be
        placed in outputs. If True, 'For Investigational Use Only' phrase placed in
        PDF report and output series descriptions.

        ithresh (`int`): inspiration threshold

        ethresh (`int`): expiration threshold

        set_info (`set_integration.SETinfo`): Contains information on
        such as SET version to be used in populating DICOM metadata
        if SET editor was used on input segmentation.

        filter_on (`bool`): True if median filtering was used

        language_tag (`string`): Language string to append to SeriesDescription

    # Returns
        An Encapsulated PDF Storage `DicomSeries` with the needed DICOM
        metadata tags populated and PDF report encapsulated.
    """
    out_dcm_series = dhd_gos.create_pdf_dicom(ct_series, pdfname)
    out_dcm_series = dicom_tags.populate_lda_dicom_metadata(
        out_dcm_series,
        ct_series,
        report_type,
        DicomOutputType.PDF_REPORT,
        research_label,
        set_info,
        ithresh,
        ethresh,
        filter_on,
        report_type.is_select,
        language_tag,
        fc_overread,
    )

    # Before returning dicom report, assert patient attributes match original
    dicom_tags.assert_patient_attributes_match(
        ct_series, out_dcm_series, ("PatientName", "PatientID", "StudyInstanceUID")
    )

    return out_dcm_series


def create_secondary_capture(
    pdfname,
    ct_series,
    report_type,
    research_label,
    ithresh,
    ethresh=None,
    set_info=None,
    filter_on=None,
    language_tag=None,
    fc_overread=None,
):
    """Creates SOP Class for secondary capture report

    # Arguments

        pdfname: path to directory of saved pdf report

        ct_series (`DicomSeries`): input inspiration image

        report_type (`ReportType`): indicates type of report generated.

        research_label (`bool`): Flag indicating research labelling to be
        placed in outputs. If True, 'For Investigational Use Only' phrase placed in
        PDF report and output series descriptions.

        ithresh (`int`): inspiration threshold

        ethresh (`int`): expiration threshold

        set_info (`set_integration.SETinfo`): Contains information on
        such as SET version to be used in populating DICOM metadata
        if SET editor was used on input segmentation.

        language_tag (`string`): Language string to append to SeriesDescription

    # Returns
        A Secondary Capture Image Storage `DicomSeries` object with the needed
        DICOM metadata tags populated and PDF report as pixel data.
    """
    try:
        out_dcm_series = dhd_gos.create_sc_dicom(ct_series, pdfname)
    except CalledProcessError as err:
        lda_logger.debug(err.stdout)
        # Raise error - BMP conversion failed
        codes.raise_code("0006", "PDF to BMP conversion failed")
    out_dcm_series = dicom_tags.populate_lda_dicom_metadata(
        out_dcm_series,
        ct_series,
        report_type,
        DicomOutputType.SC_REPORT,
        research_label,
        set_info,
        ithresh,
        ethresh,
        filter_on,
        report_type.is_select,
        language_tag,
        fc_overread,
    )

    # Before returning dicom report, assert patient attributes match original
    dicom_tags.assert_patient_attributes_match(
        ct_series, out_dcm_series, ("PatientName", "PatientID", "StudyInstanceUID")
    )

    return out_dcm_series
