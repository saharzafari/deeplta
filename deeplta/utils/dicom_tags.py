"""
Copyright (c) Imbio. All rights reserved.

dicom_tags.py - LDA DICOM related functions
"""
from dicomhd import data_structures as dhd_ds
from dicomhd import generate_output_series as dhd_gos
from pydicom.dataset import Dataset

from . import codes
from .definitions import ALG_VERSION


def populate_lda_dicom_metadata(
    new_series,
    old_series,
    series_type,
    output_type,  # pylint:disable=too-many-arguments
    research,
    set_info=None,
    ithresh=None,
    ethresh=None,
    filter_on=None,
    is_select=False,
    language_tag=None,
    fc_overread=None,
):
    """Populates DICOM tags of new_series that are required in all LTA outputs.

    Where appropriate, copies metadata from old_dcm; otherwise, populates with
    new information.

    # Arguments
        new_series (`dicomhd.DicomSeries`): output image series

        old_series (`dicomhd.DicomSeries`): input image series

        series_type (`ReportType` or `SegmentationType`): type of series or
        segmentation that this metadata is for.

        output_type (`DicomOutputType`): Type of DICOM output being produced.

        research (`bool`): Flag indicating research usage. If True,
        'For Investigational Use Only' phrase placed in output series descriptions.

        set_info (`set_integration.SETinfo`): contains SET info extracted from
        input series

        ithresh (`int`): inspiration threshold.

        ethresh (`int`): expiration threshold. Required for PRM outputs.

        filter_on (`bool`): True if median filtering was used

        is_select (`bool`): True if SeriesDescription should
        begin with "SeleCT" instead of "LDA"

        language_tag (`string`): Language string to append to SeriesDescription

    # Returns
        Input `new_series` object, with populated DICOM fields
    """
    # If private tag with name of segmentation editor is present, '*EDITED*' will be
    #  included in the series description.
    # If the report is being generated in the US or the algorithm is being run in research
    #  mode, disclaimer 'For Investigational Use Only' is required in the series description.
    add_disclaimer = research

    if set_info is not None and set_info.editing_software == "SET":
        new_series = populate_set_private_tag(new_series, set_info)
    new_series = populate_series_information(
        new_series,
        old_series,
        series_type,
        output_type,
        set_info,
        add_disclaimer,
        ithresh,
        ethresh,
        filter_on,
        language_tag,
        is_select=is_select,
        fc_overread=fc_overread,
    )

    return new_series


def populate_set_private_tag(new_series, set_info):
    """Populate SET version number from _set_version value in lda_logger.

    # Arguments
        new_series (`dicomhd.DicomSeries`): Series to populate

        set_info (`set_integration.SETinfo`): contains SET info extracted from
        input series

    # Returns
        Input `new_dcm` object, with private DICOM tags populated with SET info
    """
    set_private_tags = [
        dhd_ds.PrivateTagInfo(tag=[0x0011, 0x0011], VR="LO", value="SET Version"),
        dhd_ds.PrivateTagInfo(tag=[0x0011, 0x1100], VR="LO", value=set_info.version),
    ]
    new_series = dhd_ds.update_private_tags(new_series, set_private_tags)
    return new_series


def populate_series_information(
    new_series,
    old_series,
    series_type,
    output_type,
    set_info,
    add_disclaimer,
    ithresh,
    ethresh,
    filter_on,
    language_tag=None,
    is_select=False,
    fc_overread=None,
):
    """Populates series-related DICOM tags of new_series from old_series.

    # Arguments
        new_series (`pydicom.Dataset`): Series to populate with metadata

        old_series (`function`): Input series to pull metadata from

        series_type (`ReportType` or `SegmentationType`): type of series or
        segmentation that this metadata is for.

        output_type (`DicomOutputType`): Type of DICOM output being produced.

        set_info (`set_integration.SETinfo`): contains SET info extracted from
        input series

        add_disclaimer (`bool`): True if 'For Investigational Use Only' disclaimer
        should be added to SeriesDescription.

        ithresh (`int`): inspiration threshold.

        ethresh (`int`): expiration threshold. Required for PRM outputs.

        filter_on (`bool`): True if median filtering was used

        language_tag (`string`): Language string to append to SeriesDescription

        is_select (`bool`): True if SeriesDescription should
        begin with "SeleCT" instead of "LDA"

    # Returns
        Input `new_dcm` object, with series-related DICOM fields populated
    """
    run_count = "0" if set_info is None else str(set_info.run_count)

    # Series number
    series_prefix = get_series_prefix(series_type, output_type)
    old_series_number = str(dhd_ds.get_dicom_value(old_series, "SeriesNumber"))
    series_number = series_prefix + old_series_number + run_count

    # Series Description
    series_desc = create_series_description(
        series_type,
        output_type,
        set_info,
        add_disclaimer,
        is_select=is_select,
        language_tag=language_tag,
    )

    # Derivation Description
    deriv_desc = create_derivation_description(
        series_type,
        set_info,
        ithresh,
        ethresh,
        filter_on,
        fc_overread=fc_overread,
    )

    new_meta = {
        "SeriesNumber": series_number,
        "SeriesDescription": series_desc,
        "DerivationDescription": deriv_desc,
        "ImageComments": "Imbio CT Lung Density Analysis",
    }

    new_series = dhd_ds.update_meta(new_series, new_meta)

    return new_series


def create_series_description(
    series_type,
    output_type,
    set_info,
    add_disclaimer,
    is_select=False,
    language_tag=None,
):
    """Generates series description for series based on the input parameters.

    The basic format for LDA series descriptions is
    '<LDA Func/LDA Insp/SeleCT> <Report/Raw/RGB> v<version>'.
    There are also other types of Reports than Func/Insp, and there are optional
    additions of '*EDITED*' and ' - For Investigational Use Only'.
    Two possible series descriptions are:
      'LDA Func RGB v3.0.1'
      'LDA LungMap NonSmoker *EDITED* Report v3.0.1 - For Investigational Use Only'

    # Arguments
        series_type (`ReportType` or `SegmentationType`): type of series or
        segmentation that this metadata is for.

        output_type (`DicomOutputType`): Type of DICOM output being produced.

        set_info (`set_integration.SETinfo`): contains SET info extracted from
        input series

        add_disclaimer (`bool`): True if 'For Investigational Use Only' disclaimer
        should be added to SeriesDescription.

        is_select (`bool`): True if SeriesDescription should
        begin with "SeleCT" instead of "LDA"

        language_tag (`string`): Language string to append to SeriesDescription

    # Returns
        A `str` to set as the output series SeriesDescription
    """
    assessment = series_type.assessment_name
    if is_select:
        assessment = assessment.replace("LDA", "SeleCT")
    output_type_name = output_type.output_type_name

    edited = "*EDITED* " if set_info is not None else ""
    research_disclaimer = " - For Investigational Use Only" if add_disclaimer else ""

    series_desc = (
        f"{assessment} {edited}{output_type_name} "
        f"v{ALG_VERSION}{research_disclaimer}"
    )

    if language_tag:
        series_desc += f" - {language_tag}"

    return series_desc


def create_derivation_description(
    series_type,
    set_info,
    ithresh=None,
    ethresh=None,
    filter_on=None,
    fc_overread=None,
):
    """Generates derivation description for series based on the input
    parameters.

    # Arguments
        series_type (`ReportType` or `SegmentationType`): type of series or
        segmentation that this metadata is for.

        set_info (`set_integration.SETinfo`): contains SET info extracted from
        input series

        filter_on (`bool`): True if median filtering was applied to the images

        ithresh (`int`): inspiration threshold. Required for non-segmentation
        outputs.

        ethresh (`int`): expiration threshold. Required for PRM outputs.

    # Returns
        A `str`to set as the output series DerivationDescription
    """

    filter_bool2str = {
        None: "",
        False: " Noise filtering: OFF.",
        True: " Noise filtering: ON.",
    }

    derivation_description = series_type.derivation_description
    derivation_description = derivation_description.format(
        ithresh=ithresh, ethresh=ethresh, filter_status=filter_bool2str[filter_on]
    )

    if set_info is not None and set_info.editing_software == "SET":
        derivation_description += (
            " Segmentation manually edited using Imbio SET software."
        )
    elif (
        set_info is not None
        and set_info.editing_software == "SegmentationStation"
        and fc_overread
    ):
        derivation_description += " These fissure integrity values have been overread and verified by a board-certified radiologist."
    elif set_info is not None and set_info.editing_software == "SegmentationStation":
        derivation_description += (
            " Segmentation manually edited using Imbio Segmentation Station software."
        )

    # Make sure derivation description does not exceed maximum length
    if len(derivation_description) > 1024:
        codes.raise_code(
            "0005",
            "Generated derivation description length " "exceeds 1024 characters.",
        )

    return derivation_description


def get_series_prefix(series_type, output_type):
    """Returns series prefix for the specific output.

    # Arguments
        series_type (`ReportType` or `SegmentationType`): type of series or
        segmentation that this metadata is for.

        output_type (`DicomOutputType`): Type of DICOM output being produced.

    # Returns
        A 3-digit `str` prefix to prepend to output series number
    """
    output_part = output_type.series_prefix_key
    series_part = series_type.series_prefix_key
    series_prefix = f"{output_part}{series_part}"
    return series_prefix


def assert_patient_attributes_match(dcm1, dcm2, attributes=None):
    """Assert patient attributes match between DICOM metadata sets

    By default, checks the following attributes:
        PatientName
        PatientID
        StudyInstanceUID

    # Arguments
        dcm1 (`DicomSeries` or `Dataset`): First dicom image to check
        dcm2 (`DicomSeries` or `Dataset`): Second dicom image to check
        attributes (`list`): Attributes to verify

    # Raises
        `AlgorithmFailedException` if specified attributes between the two
        datasets do not match
    """
    if not attributes:
        attributes = ("PatientName", "PatientID", "StudyInstanceUID")
    # Handle both Dataset and dicomhd object types
    get_dcm1_values = _generate_get_dcm_values_function(dcm1)
    get_dcm2_values = _generate_get_dcm_values_function(dcm2)
    for attribute in attributes:
        # Note: Consider empty string and None as equivalent by filtering them out
        dcm1_values = [
            val for val in get_dcm1_values(attribute) if val not in (None, "")
        ]
        dcm2_values = [
            val for val in get_dcm2_values(attribute) if val not in (None, "")
        ]
        if not dcm1_values == dcm2_values:
            codes.raise_code("0005", "Output attributes do not match original")


def _generate_get_dcm_values_function(dcm_file):
    """Returns a function to get DICOM values based on whether
    `dcm_file` is a pydicom or dicomhd object.
    """
    if isinstance(dcm_file, Dataset):

        def get_dcm_values_function(tag):
            value = dcm_file.get(tag, None)
            values_tuple = (value,)
            return values_tuple

    else:

        def get_dcm_values_function(tag):
            try:
                values, _ = dhd_ds.get_dicom_values(dcm_file, tag)
            except AttributeError:
                values = ()
            return values

    return get_dcm_values_function


def get_default_window_levels(mask_data):
    """WindowCenter and WindowWidth can be set in a DICOM viewer to adjust how
    the image is displayed. For non-RGB-outputs, default values for these help
    show the different values in the mask more clearly upon opening a file.

    # Arguments
        mask_data (`numpy.ndarray`): Pixel data for which to calculate default
        window levels. Maximum value will determine window level values.

    # Returns
        A `dict` with WindowWidth and WindowCenter as keys,
        for passing into `dicomhd.data_structures.update_metadata()`
    """
    return dhd_gos._get_default_window_levels(mask_data)


def is_default_z_orientation(dcm_series):
    """The default z-orientation for a DicomSeries is an increasing z-value
    for ImagePositionPatient throughout the image. If it is decreasing, then
    the output pngs and image overlays need to be flipped differently to
    reflect that.
    """
    patientdata_slice_0 = dcm_series.children[0].children[0]
    patientdata_slice_1 = dcm_series.children[1].children[0]
    zpos_0 = patientdata_slice_0.ImagePositionPatient[2]
    zpos_1 = patientdata_slice_1.ImagePositionPatient[2]
    z_dir = zpos_1 - zpos_0
    if z_dir < 0:
        return False
    else:
        return True
