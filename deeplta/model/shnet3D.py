import tensorflow as tf
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv3D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import GlobalAveragePooling3D
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import MaxPooling3D
from tensorflow.keras.layers import UpSampling3D
from tensorflow.keras.layers import concatenate
from tensorflow.keras.models import Model


class ShadowNet(object):
    _name = 'shnet'

    def __init__(self,
                 img_plane, img_width, img_height, n_channels,
                 n_filters=16,
                 filter_size=(3, 3, 3),
                 pool_size=(2, 2, 2),
                 max_pooling=True,
                 batch_normalization=True,
                 dilation_rate=1,
                 n_classes=6,
                 class_names=('1', '2', '3', '4', '5', '6'),
                 ):
        self.img_plane = img_plane
        self.img_width = img_width
        self.img_height = img_height
        self.n_channels = n_channels
        self.n_filters = n_filters
        self.filter_size = filter_size
        self.pool_size = pool_size
        self.max_pooling = max_pooling
        self.batch_normalization = batch_normalization
        self.dilate_rate = dilation_rate
        self.n_classes = n_classes
        self.class_names = class_names

        self._init_model()

    def _init_model(self):

        # Define input batch shape
        inputs = Input(batch_shape=(None, self.img_plane, self.img_width, self.img_height, self.n_channels), name='input')
        # conv-block 1
        n_block_convolutions = 2
        filter_size_coeff = 2 ** 0
        conv1 = inputs
        for i in range(n_block_convolutions):
            conv1 = Conv3D(self.n_filters * filter_size_coeff,
                           self.filter_size,
                           activation='relu',
                           padding='same',
                           dilation_rate=self.dilate_rate,
                           name=f'block1_conv{i + 1}'
                           )(conv1)
            if self.batch_normalization:
                conv1 = BatchNormalization(name=f'block1_bn{i + 1}')(conv1)
        if self.max_pooling:
            conv1 = MaxPooling3D(pool_size=self.pool_size,
                                 data_format='channels_last',
                                 name='block1_mp'
                                 )(conv1)
        # conv-block 2
        n_block_convolutions = 2
        filter_size_coeff = 2 ** 1
        conv2 = conv1
        for i in range(n_block_convolutions):
            conv2 = Conv3D(self.n_filters * filter_size_coeff,
                           self.filter_size,
                           activation='relu',
                           padding='same',
                           dilation_rate=self.dilate_rate,
                           name=f'block2_conv{i + 1}',
                           )(conv2)
            if self.batch_normalization:
                conv2 = BatchNormalization(name=f'block2_bn{i + 1}')(conv2)
        if self.max_pooling:
            conv2 = MaxPooling3D(pool_size=self.pool_size,
                                 data_format='channels_last',
                                 name='block2_mp'
                                 )(conv2)
        # conv-block 3
        n_block_convolutions = 3
        filter_size_coeff = 2 ** 2
        conv3 = conv2
        for i in range(n_block_convolutions):
            conv3 = Conv3D(self.n_filters * filter_size_coeff,
                           self.filter_size,
                           activation='relu',
                           padding='same',
                           dilation_rate=self.dilate_rate,
                           name=f'block3_conv{i + 1}'
                           )(conv3)
            if self.batch_normalization:
                conv3 = BatchNormalization(name=f'block3_bn{i + 1}')(conv3)
        if self.max_pooling:
            conv3 = MaxPooling3D(pool_size=self.pool_size,
                                 data_format='channels_last',
                                 name='block3_mp')(conv3)
        # conv-block 4
        n_block_convolutions = 3
        filter_size_coeff = 2 ** 3
        conv4 = conv3
        for i in range(n_block_convolutions):
            conv4 = Conv3D(self.n_filters * filter_size_coeff,
                           self.filter_size,
                           activation='relu',
                           padding='same',
                           dilation_rate=self.dilate_rate,
                           name=f'block4_conv{i + 1}'
                           )(conv4)
            if self.batch_normalization:
                conv4 = BatchNormalization(name=f'block4_bn{i + 1}')(conv4)
        if self.max_pooling:
            conv4 = MaxPooling3D(pool_size=self.pool_size,
                                 data_format='channels_last',
                                 name='block4_mp'
                                 )(conv4)
        # conv-block 5
        n_block_convolutions = 3
        filter_size_coeff = 2 ** 3
        conv5 = conv4
        for i in range(n_block_convolutions):
            conv5 = Conv3D(self.n_filters * filter_size_coeff,
                           self.filter_size,
                           activation='relu',
                           padding='same',
                           dilation_rate=self.dilate_rate,
                           name=f'block5_conv{i + 1}'
                           )(conv5)
            if self.batch_normalization:
                conv5 = BatchNormalization(name=f'block5_bn{i + 1}')(conv5)
        # 6:
        filter_size_coeff = 2 ** 2
        conv6 = Conv3D(self.n_filters * filter_size_coeff,
                       (1, 1),
                       activation='relu',
                       padding='same',
                       dilation_rate=self.dilate_rate,
                       name='block6_conv')(conv5)

        # 8: final decision (activation)
        outputs = GlobalAveragePooling3D(name='block8_gavp')(conv6)
        # 9: Softmax
        outputs = Dense(self.n_classes, name='block9_softmax', activation='softmax')(outputs)
        self.model = Model(inputs=inputs, outputs=outputs)

    def compile(self, optimizer='adam',
                loss='tf.keras.losses.CategoricalCrossentropy(from_logits=False)',
                metrics=['accuracy']):
        self.model.compile(
            optimizer=optimizer,
            loss=eval(loss),
            metrics=metrics
        )

    def fit(self,
            train_dir,
            valid_dir,
            n_epochs,
            batch_size,
            steps_per_epoch=None,
            data_gen_params=dict(rescale=1. / 255),
            callbacks=[], ):

        data_gen = tf.keras.preprocessing.image.ImageDataGenerator(**data_gen_params)
        train_generator = data_gen.flow_from_directory(
            train_dir,
            target_size=(self.img_width, self.img_height),
            batch_size=batch_size,
            classes=self.class_names,
            color_mode='grayscale',
            class_mode='categorical',
        )
        valid_generator = data_gen.flow_from_directory(
            valid_dir,
            target_size=(self.img_width, self.img_height),
            batch_size=batch_size,
            classes=self.class_names,
            color_mode='grayscale',
            class_mode='categorical',
        )

        if steps_per_epoch is None:
            steps_per_epoch = train_generator.samples // batch_size
        return self.model.fit(
            train_generator,
            steps_per_epoch=steps_per_epoch,
            validation_data=valid_generator,
            epochs=n_epochs,
            callbacks=callbacks
        )

    def predict(self, *args, **kwargs):
        return self.model.predict(*args, **kwargs)

    def evaluate(self, *args, **kwargs):
        return self.model.evaluate(*args, **kwargs)

    def summary(self):
        self.model.summary()

    def load_weights(self, fname):
        self.model.load_weights(fname)

    def save_weights(self, fname, overwrite=True):
        self.model.save_weights(fname, overwrite=overwrite)


def get_unet(img_width, img_height, n_channels,
             n_filters=16,
             batch_normalization=True,
             dilation_rate=1,
             n_classes=2):
    # Define input batch shape
    batch_shape = (img_width, img_height, n_channels)
    inputs = Input(batch_shape=(None, img_width, img_height, n_channels))
    # block 1
    conv1 = Conv2D(n_filters * 1, (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block1-conv1')(inputs)
    if batch_normalization:
        conv1 = BatchNormalization(name='block1-bn1')(conv1)
    conv1 = Conv2D(n_filters * 1, (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block1-conv2')(conv1)
    if batch_normalization:
        conv1 = BatchNormalization(name='block1-bn2')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2),
                         data_format='channels_last',
                         name='block1-mp1')(conv1)
    # block 2
    conv2 = Conv2D(n_filters * 2,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block2-conv1')(pool1)
    if batch_normalization:
        conv2 = BatchNormalization(name='block2-bn1')(conv2)
    conv2 = Conv2D(n_filters * 2,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block2-conv2')(conv2)
    if batch_normalization:
        conv2 = BatchNormalization(name='block2-bn2')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2),
                         data_format='channels_last',
                         name='block2-mp1')(conv2)
    # block 3
    conv3 = Conv2D(n_filters * 4,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block3-conv1')(pool2)
    if batch_normalization:
        conv3 = BatchNormalization(name='block3-bn1')(conv3)
    conv3 = Conv2D(n_filters * 4,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block3-conv2')(conv3)
    if batch_normalization:
        conv3 = BatchNormalization(name='block3-bn2')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2),
                         data_format='channels_last',
                         name='block3-mp')(conv3)
    # block 4
    conv4 = Conv2D(n_filters * 8,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block4-conv1')(pool3)
    if batch_normalization:
        conv4 = BatchNormalization(name='block4-bn1')(conv4)
    conv4 = Conv2D(n_filters * 8,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block4-conv2')(conv4)
    if batch_normalization:
        conv4 = BatchNormalization(name='block-bn2')(conv4)

    pool4 = MaxPooling2D(pool_size=(2, 2),
                         data_format='channels_last',
                         name='block4-mp1')(conv4)
    # block 5
    conv5 = Conv2D(n_filters * 16,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block5-conv1')(pool4)
    if batch_normalization:
        conv5 = BatchNormalization(name='block5-bn1')(conv5)
    conv5 = Conv2D(n_filters * 16,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block5-conv2')(conv5)
    if batch_normalization:
        conv5 = BatchNormalization(name='block5-bn2')(conv5)
    # skip connection
    up6 = concatenate([UpSampling2D(size=(2, 2), name='block5-us1')(conv5), conv4], axis=3)
    # block 6
    conv6 = Conv2D(n_filters * 8,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block6-conv1')(up6)
    if batch_normalization:
        conv6 = BatchNormalization(name='block6-bn1')(conv6)
    conv6 = Conv2D(n_filters * 8,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block6-conv2')(conv6)
    if batch_normalization:
        conv6 = BatchNormalization(name='block6-bn2')(conv6)
    # skip connection
    up7 = concatenate([UpSampling2D(size=(2, 2), name='block6-us1')(conv6), conv3], axis=3)
    # block 7
    conv7 = Conv2D(n_filters * 4,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block7-conv1')(up7)
    if batch_normalization:
        conv7 = BatchNormalization(name='block7-bn1')(conv7)
    conv7 = Conv2D(n_filters * 4,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block7-conv2')(conv7)
    if batch_normalization:
        conv7 = BatchNormalization(name='block7-bn2')(conv7)
    # skip connection
    up8 = concatenate([UpSampling2D(size=(2, 2), name='block7-us1')(conv7), conv2], axis=3)
    # block 8
    conv8 = Conv2D(n_filters * 2,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block8-conv1')(up8)
    if batch_normalization:
        conv8 = BatchNormalization(name='block8-bn1')(conv8)
    conv8 = Conv2D(n_filters * 2,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block8-cnv2')(conv8)
    if batch_normalization:
        conv8 = BatchNormalization(name='block8-bn2')(conv8)
    # skip connection
    up9 = concatenate([UpSampling2D(size=(2, 2), name='block8-us1')(conv8), conv1], axis=3)
    # block 9
    conv9 = Conv2D(n_filters * 1,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block9-conv1')(up9)
    if batch_normalization:
        conv9 = BatchNormalization(name='bock9-bn1')(conv9)
    conv9 = Conv2D(n_filters * 1,
                   (3, 3),
                   activation='relu',
                   padding='same',
                   dilation_rate=dilation_rate,
                   name='block9-conv2')(conv9)
    if batch_normalization:
        conv9 = BatchNormalization(name='block9-bn2')(conv9)
    # block 10: feature map
    conv10 = Conv2D(n_classes,
                    (1, 1),
                    activation='softmax',
                    padding='same',
                    dilation_rate=dilation_rate,
                    name='block10-featuremap'
                    )(conv9)
    # block 11: classifier
    outputs = Flatten()(conv10)
    #     outputs = Dense(64, activation='relu', name='block11-dense')(outputs)
    outputs = Dense(n_classes, name='block11-logits')(outputs)
    model = Model(inputs=inputs, outputs=outputs)
    return model
