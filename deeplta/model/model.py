
from tensorflow.keras.models import Model
from deeplta.model import BaseModel
from deeplta.model.resnet2D import Resnet2DBuilder


class DeepLTA(BaseModel):

    _name = 'deeplta'

    def __init__(self, config):

        super(DeepLTA, self).__init__(config)

    def _build_model(self) -> Model:

        model = Resnet2DBuilder.build_resnet_05(
            self.config.model['input_shape'],
            self.config.model['n_classes']
        )
        return model




