
from abc import ABC
from abc import abstractmethod
import tensorflow as tf
from tensorflow.keras.models import Model


class BaseModel(ABC):
    _name = 'base_model'

    def __init__(self, config):

        self.config = config
        self._metrics = None
        self._optimizer = None
        self._loss = None
        self.model = self._build_model()

    @abstractmethod
    def _build_model(self) -> Model:
        return Model

    def compile(self):
        self._metrics = [eval(metric) for metric in self.config.train.metrics]
        self._optimizer = getattr(tf.keras.optimizers,
                                  self.config.train.optimizer.type.title())(
            **self.config.train.optimizer.params.__dict__
        )
        self._loss = getattr(tf.keras.losses, self.config.train.loss.type.lower())

        self.model.compile(
            optimizer=self._optimizer,
            loss=self._loss,
            metrics=self._metrics
        )
        print('compile completed.')

    def fit(self,
            train_data,
            valid_data,
            n_epochs,
            steps_per_epoch=None,
            validation_steps=None,
            callbacks=[],):


        return self.model.fit(
            train_data,
            validation_data=valid_data,
            steps_per_epoch=steps_per_epoch,
            validation_steps=validation_steps,
            epochs=n_epochs,
            callbacks=callbacks
        )

    def predict(self, *args, **kwargs):
        return self.model.predict(*args, **kwargs)

    def evaluate(self, *args, **kwargs):
        return self.model.evaluate(*args, **kwargs)

    def summary(self):
        self.model.summary()

    def load_weights(self, fname):
        self.model.load_weights(fname)

    def save_weights(self, fname, overwrite=True):
        self.model.save_weights(fname, overwrite=overwrite)

    def load_tfrecord_data(self):
        pass

    def load_train_data(self):
        raise NotImplementedError()

    def load_valid_data(self):
        raise NotImplementedError()